//==========================================
// Author: Michael Guldborg
// Date:   Jan 2021
//==========================================
package org.acme.resources;

import org.acme.businesslogic.RefundService;
import org.acme.models.RefundRequest;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/refund")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.TEXT_PLAIN)
public class RefundResource {
    @POST
    public Response refundPayment(@RequestBody RefundRequest request) {
        RefundService service = RefundService.getInstance();
        return service.refundPayment(request);
    }
}


