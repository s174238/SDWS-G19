/*
 * @author Sebastian Kotewitz s174270
 */

package moneytransfer;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class TokenService implements EventReceiver {

    EventSender sender;

    public TokenService(EventSender sender) {
        this.sender = sender;
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        TokenServiceCommands tokenServiceCommands = new TokenServiceCommands();
        try {
            tokenServiceCommands.executeCommand(event, sender);
        } catch (Exception ex) {
            sender.sendEvent(new Event(event.getEventType() + "Error", new String[]{"Invalid or missing arguments"}));
        }
    }
}
