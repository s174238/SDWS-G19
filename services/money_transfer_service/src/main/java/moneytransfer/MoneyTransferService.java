package moneytransfer;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import java.math.BigDecimal;

import dtu.bank.*;


/**
* @author s174238 Mads Christensen
*/
public class MoneyTransferService implements EventReceiver {

	EventSender sender;
	BankService bank;

	public MoneyTransferService(EventSender sender) {
		bank = new BankServiceService().getBankServicePort();
		this.sender = sender;
	}
	
	public void transferMoney(Event event) throws BankServiceException_Exception {
		// Split
		String debtor = (String) event.getArguments()[0];
		String cred = (String) event.getArguments()[1];
		BigDecimal amount = BigDecimal.valueOf((double) event.getArguments()[2]);
		String description = (event.getEventType().equals("RefundRequestTransactionConverted")) ? "Refund" : (String) event.getArguments()[3];
		
		bank.transferMoneyFromTo(debtor, cred, amount, description);
	}
	
	@Override
	public void receiveEvent(Event event) throws Exception {
		if (event.getEventType().equals("RequestValidatedConverted")) {
			System.out.println("event handled: "+event);
		
			try {
				transferMoney(event);
				// Send event
				Event e = new Event("MoneyTransferred");
				sender.sendEvent(e);
				// Log
				Event logEvent = new Event("LogTransaction", new Object[] {event.getArguments()[4], event.getArguments()[5], event.getArguments()[2]});
				sender.sendEvent(logEvent);
			} catch (BankServiceException_Exception e) {
				Object[] m = {e.getMessage()};
				sender.sendEvent(new Event("MoneyTransferFailed", m));
			}
		} else if (event.getEventType().equals("RefundRequestTransactionConverted")) {
			try {
				transferMoney(event);
				// Send event
				Event e = new Event("RefundTransferred");
				sender.sendEvent(e);
				// Log 
				Event logEvent = new Event("LogTransaction", new Object[] {event.getArguments()[3], event.getArguments()[4], event.getArguments()[2]});
				sender.sendEvent(logEvent);
			} catch (BankServiceException_Exception e) {
				Object[] m = {e.getMessage()};
				sender.sendEvent(new Event("RefundTransferFailed", m));
			}
		} else {
			System.out.println("event ignored: "+event);
		}
	}

}
