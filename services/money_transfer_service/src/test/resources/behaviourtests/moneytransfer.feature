Feature: Money Transfer feature

  Scenario: Successful transfer
  	Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is registered with DTUPay
    When I receive event "RequestValidatedConverted" with customer, merchant and amount "10"
    Then transfer succeeds
    And the balance of the customer at the bank is "990.0" kr
		And the balance of the merchant at the bank is "2010.0" kr
    Then I have sent event "MoneyTransferred"
    And I have sent event "LogTransaction"
   
	Scenario: Failed transfer - negative balance
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is registered with DTUPay
    When I receive event "RequestValidatedConverted" with customer, merchant and amount "1001"
    Then transfer fails
    And I have sent event "MoneyTransferFailed"
    And event contains error "Debtor balance will be negative"
    
	Scenario: Failed transfer - negative amount
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is registered with DTUPay
    When I receive event "RequestValidatedConverted" with customer, merchant and amount "-1"
    Then transfer fails
    And I have sent event "MoneyTransferFailed"
    And event contains error "Amount must be positive"
    
 	Scenario: Failed transfer - debtor not in bank
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is not registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is registered with DTUPay
    When I receive event "RequestValidatedConverted" with customer, merchant and amount "10"
    Then transfer fails
    And I have sent event "MoneyTransferFailed"
    And event contains error "Debtor account does not exist"   
 
	Scenario: Failed transfer - creditor not in bank
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is not registered with DTUPay
    When I receive event "RequestValidatedConverted" with customer, merchant and amount "10"
    Then transfer fails
    And I have sent event "MoneyTransferFailed"
    And event contains error "Creditor account does not exist"   
       
	Scenario: Successful refund
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is registered with DTUPay
    When I receive event "RefundRequestTransactionConverted" with customer, merchant and amount "10"
    Then transfer succeeds
    And I have sent event "RefundTransferred"
    And I have sent event "LogTransaction"
    And the balance of the customer at the bank is "1010.0" kr
		And the balance of the merchant at the bank is "1990.0" kr
		
	Scenario: Failed refund - negative balance
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is registered with DTUPay
    When I receive event "RefundRequestTransactionConverted" with customer, merchant and amount "2001"
    Then transfer fails
    And I have sent event "RefundTransferFailed"
    And event contains error "Debtor balance will be negative"
    And the balance of the customer at the bank is "1000" kr
		And the balance of the merchant at the bank is "2000" kr
    
	Scenario: Failed refund - negative amount
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is registered with DTUPay
    When I receive event "RefundRequestTransactionConverted" with customer, merchant and amount "-1"
    Then transfer fails
    And I have sent event "RefundTransferFailed"
    And event contains error "Amount must be positive"
    And the balance of the customer at the bank is "1000" kr
		And the balance of the merchant at the bank is "2000" kr
    
 	Scenario: Failed refund - Creditor not in bank
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is not registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is registered with DTUPay
    When I receive event "RefundRequestTransactionConverted" with customer, merchant and amount "10"
    Then transfer fails
    And I have sent event "RefundTransferFailed"
    And event contains error "Creditor account does not exist"
		And the balance of the merchant at the bank is "2000" kr
 
	Scenario: Failed refund - Debtor not in bank
    Given the customer "a" "b" with CPR "++++++-++++c" has a bank account
		And the balance of customer account is 1000
		And the customer is registered with DTUPay
		And the merchant "------1" "---------1" with CPR number "******-****1" has a bank account
		And the balance of merchant account is 2000
		And the merchant is not registered with DTUPay
    When I receive event "RefundRequestTransactionConverted" with customer, merchant and amount "10"
    Then transfer fails
    And I have sent event "RefundTransferFailed"
    And event contains error "Debtor account does not exist"
    And the balance of the customer at the bank is "1000" kr
