# Sebastian Tønne Kotewitz s174270

Feature: Get tokens from token

  Scenario: Get 5 tokens when 1 token is present
    Given a user exists
    And the user has 1 tokens
    When I receive event "GiveTokensToken" with the token and the number 5
    Then I have sent event "GiveTokensTokenResponse" with the 6 tokens and the user's ID
    And the user now has 6 tokens

  Scenario: Get 7 tokens when 1 token is present
    Given a user exists
    And the user has 1 tokens
    When I receive event "GiveTokensToken" with the token and the number 7
    Then I have sent event "GiveTokensTokenError" with the message "Can only request max 5 tokens at once"
    And the user now has 1 tokens

  Scenario: Get 4 tokens when 2 tokens are present
    Given a user exists
    And the user has 2 tokens
    When I receive event "GiveTokensToken" with the token and the number 4
    Then I have sent event "GiveTokensTokenError" with the message "Can only request tokens when one or zero tokens remain"
    And the user now has 2 tokens

  Scenario: Get 5 tokens when token is invalid
    Given a token exists
    When I receive event "GiveTokensToken" with the token and the number 5
    Then I have sent event "GiveTokensTokenError" with the message "Invalid token"
