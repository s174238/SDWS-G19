/*
 * @author Sebastian Kotewitz s174270
 */
package moneytransfer;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

public class ReportService implements EventReceiver {

    EventSender sender;
    ReportServiceCommands commands = new ReportServiceCommands();

    public ReportService(EventSender sender) {
        this.sender = sender;
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        try {
            commands.executeCommand(event, sender);
        } catch (Exception ex) {
            sender.sendEvent(new Event(event.getEventType() + "Error", new String[]{"Invalid or missing arguments"}));
        }


    }

}
