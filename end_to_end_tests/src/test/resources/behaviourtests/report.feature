#==========================================
# Author: Sebastian
# Date:   Jan 2021
#==========================================
Feature: Report

  Scenario: Report all
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    And the costumer requests 1 tokens via id

    Given a new merchant has a bank account
    When the merchant registers with their Bank Account ID as merchant

    And the costumer wants to pay 100 kr to the merchant for a "product"
    And the costumer requests 1 tokens via id
    And the costumer wants to pay 200 kr to the merchant for a "product2"

    Given a brand new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    And the costumer requests 1 tokens via id
    And the costumer wants to pay 500 kr to the merchant for a "product3"

    And sleep for 2 seconds

    And the admin requests a full report
    Then the list of transactions contains a transaction of "100.0"
    And the list of transactions contains a transaction of "200.0"
    And the list of transactions contains a transaction of "500.0"


  Scenario: Costumer requests an empty list of transactions
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    And the costumer requests 2 tokens via id
    And the customer requests a list of transactions
    Then the list of transactions is empty

  Scenario: Customer requests an empty list of transactions with no tokens
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    And the customer requests a list of transactions
    Then the list of transactions is empty

  Scenario: Customer requests a list of transactions
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    And the costumer requests 1 tokens via id

    Given a new merchant has a bank account
    When the merchant registers with their Bank Account ID as merchant

    And the costumer wants to pay 150 kr to the merchant for a "product"
    And the costumer requests 1 tokens via id
    And the costumer wants to pay 250 kr to the merchant for a "product2"

    And sleep for 2 seconds

    And the customer requests a list of transactions
    Then the list of transactions contains a transaction of "150.0"
    And the list of transactions contains a transaction of "250.0"


  Scenario: Merchant requests an empty list of transactions
    Given a new merchant has a bank account
    When the merchant registers with their Bank Account ID as merchant

    And sleep for 2 seconds

    And the merchant requests a list of transactions
    Then the list of transactions is empty

  Scenario: Merchant requests a list of transactions
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    And the costumer requests 1 tokens via id

    Given a new merchant has a bank account
    When the merchant registers with their Bank Account ID as merchant

    And the costumer wants to pay 300 kr to the merchant for a "product"
    And the costumer requests 1 tokens via id
    And the costumer wants to pay 400 kr to the merchant for a "product2"
    
    And sleep for 2 seconds

    And the merchant requests a list of transactions
    Then the list of transactions contains a transaction of "300.0"
    And the list of transactions contains a transaction of "400.0"
