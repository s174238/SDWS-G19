//==========================================
// Author: Betta
// Date:   Jan 2021
//==========================================
package org.acme.businesslogic;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
import org.acme.models.RegisterRequest;
import org.acme.models.RegisterResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;

public class AccountService implements EventReceiver {
    private static AccountService _instance;

    public static AccountService getInstance() {
        if (_instance != null) {
            return _instance;
        }

        _instance = new AccountService(new RabbitMqSender());
        RabbitMqListener listener = new RabbitMqListener(_instance);
        try {
            listener.listen();
        } catch (Exception ignored) {
        }
        return _instance;
    }


    private EventSender sender;
    private CompletableFuture<Response> result;

    public AccountService(EventSender sender) {
        this.sender = sender;
    }

    public Response createAccount(RegisterRequest request) {
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(new Event("CreateAccount", new Object[]{request.accountId, request.type}));
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    public Response deleteAccount(String userId) {
        Event deleteAccount = new Event("DeleteAccount", new Object[]{userId});
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(deleteAccount);
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    @Override
    public void receiveEvent(Event event) {
        if (event.getEventType().equals("AccountCreated")) {
            System.out.println("event handled: " + event);
            Object[] args = event.getArguments();

            String userId = (String) args[0];
            String message = (String) args[1];

            System.out.println("UserId: " + userId);
            System.out.println("Message: " + message);

            result.complete(Response.ok().entity(new RegisterResponse(userId, message)).type(MediaType.APPLICATION_JSON).build());
        } else if (event.getEventType().equals("CreateAccountError")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity(event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        } else if (event.getEventType().equals("AccountDeleted")) {
            System.out.println("event handled: " + event);
            result.complete(Response.ok().entity(event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        } else if (event.getEventType().equals("DeleteAccountError")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity(event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        }
    }
}
