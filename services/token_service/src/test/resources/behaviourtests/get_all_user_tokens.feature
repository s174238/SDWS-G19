# Sebastian Tønne Kotewitz s174270

Feature: Get all user tokens

  Scenario: Forward customer report
    Given a user exists
    And the user has 20 invalid tokens
    And the user has 5 tokens
    When I receive event "ReportCustomer" with the customer's id
    Then I have sent event "ReportCustomerTokens" with the customer's tokens
    And the user now has 20 invalid tokens
    And the user now has 5 tokens

  Scenario: Forward customer report invalid id:
    When I receive event "ReportCustomer" with the customer's id
    Then I have sent event "ReportCustomerTokens" with an empty list of tokens
