package org.acme.models;

public class TransferRequest {
    public String token;
    public String mid;
    public double amount;
    public String desc;
}
