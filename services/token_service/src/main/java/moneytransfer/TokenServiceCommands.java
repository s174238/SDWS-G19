/*
 * @author Sebastian Kotewitz s174270
 */

package moneytransfer;

import messaging.Event;
import messaging.EventSender;

import java.math.BigDecimal;
import java.util.*;

public class TokenServiceCommands {
    private static Map<String, Command> COMMANDS;


    static {
        final Map<String, Command> commands = new HashMap<>();
        TokenServiceFunctions tokenServiceFunctions = new TokenServiceFunctions();

        commands.put("GiveTokensUserID", (event, sender) -> {
            String userID = (String) event.getArguments()[0];
            int number = ((Double) event.getArguments()[1]).intValue();
            try {
                List<UUID> tokens = tokenServiceFunctions.generateTokens(userID, number);
                Event e = new Event("GiveTokensUserIDResponse", new Object[]{userID, tokens});
                sender.sendEvent(e);
            } catch (Exception e) {
                sendError(sender, "GiveTokensUserIDError", e.getMessage());
            }
        });

        commands.put("GiveTokensToken", (event, sender) -> {
            UUID token = UUID.fromString((String) event.getArguments()[0]);
            int number = ((Double) event.getArguments()[1]).intValue();
            try {
                String userID = tokenServiceFunctions.getUserFromToken(token);
                List<UUID> tokens = tokenServiceFunctions.generateTokenWithToken(token, number);
                Event e = new Event("GiveTokensTokenResponse", new Object[]{userID, tokens});
                sender.sendEvent(e);
            } catch (Exception e) {
                sendError(sender, "GiveTokensTokenError", e.getMessage());
            }
        });

        commands.put("ReportCustomer", (event, sender) -> {
            String userID = (String) event.getArguments()[0];
            List<UUID> allTokens = tokenServiceFunctions.getAllUserTokens(userID);
            sender.sendEvent(new Event("ReportCustomerTokens", new Object[]{allTokens}));
        });

        commands.put("PaymentRequested", (event, sender) -> {
            UUID token = UUID.fromString((String) event.getArguments()[0]);
            String merchant = (String) event.getArguments()[1];
            String customer = tokenServiceFunctions.getUserFromToken(token);
            BigDecimal amount = BigDecimal.valueOf(((Double) event.getArguments()[2]));
            String desc = (String) event.getArguments()[3];
            try {
                tokenServiceFunctions.validatePayment(token);
                sender.sendEvent(new Event("RequestValidated", new Object[]{customer, merchant, amount, desc, token}));
            } catch (Exception ex) {
                sender.sendEvent(new Event("ValidationError", new Object[]{ex.getMessage()}));
            }
        });

        commands.put("RequestValidatedError", (event, sender) -> {
           sender.sendEvent(new Event("ValidationError", new Object[]{""}));
        });

        COMMANDS = Collections.unmodifiableMap(commands);
    }

    public void executeCommand(Event event, EventSender sender) throws Exception {
        Command command = COMMANDS.get(event.getEventType());

        if (command != null) {
            command.execute(event, sender);
        }
    }

    private static void sendError(EventSender sender, String error, String message) throws Exception {
        Event e = new Event(error, new String[]{message});
        sender.sendEvent(e);
    }
}
