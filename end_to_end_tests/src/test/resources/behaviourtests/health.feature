#==========================================
# Author: Christian
# Date:   Jan 2021
#==========================================
Feature: Integration

  Scenario: The api is healthy
    When the client checks the health of the api
    Then the api returns the text "healthy"

