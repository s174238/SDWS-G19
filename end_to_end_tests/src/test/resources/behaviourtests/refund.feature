#==========================================
# Author: Betta
# Date:   Jan 2021
#==========================================
#Feature: Refund
#
#  Scenario: Successful refund
#    Given a new customer has a bank account
#    When the customer registers with their Bank Account ID as customer
#    When the costumer requests 2 tokens via id
#
#    Given a new merchant has a bank account
#    When the merchant registers with their Bank Account ID as merchant
#    And the costumer wants to pay 100 kr to the merchant for a "product"
#    And sleep for 2 seconds
#
#    When the costumer want an refund from a purchase with an used token
#    Then we see the refund is successful