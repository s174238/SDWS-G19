#==========================================
# Author: Christian
# Date:   Jan 2021
#==========================================
Feature: Register

  Scenario: Register customer
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    Then the customer is registered with the Bank Account ID in DTUPay

  Scenario: Register merchant
    Given a new merchant has a bank account
    When the merchant registers with their Bank Account ID as merchant
    Then the merchant is registered with the Bank Account ID in DTUPay