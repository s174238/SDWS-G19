/*
 * @author Sebastian Kotewitz s174270
 */

package moneytransfer;

import org.glassfish.pfl.basic.contain.Triple;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ReportServiceFunctions {

    DataBank db = DataBank.getInstance();

    public void addTransactionToLog(String customerToken, String merchant, BigDecimal amount) throws Exception {
        db.addTransaction(customerToken, merchant, amount);
    }

    public List<String> convertReportList(List<Triple<String, String, BigDecimal>> list) {
        List<String> result = new ArrayList<>();
        list.forEach(l -> result.add(String.format("From: %s    To: %s    Amount: %f",
                l.first(),
                l.second(),
                l.third())));
        return result;
    }

    public List<String> reportAll() {
        List<Triple<String, String, BigDecimal>> log = db.getLog();
        return convertReportList(log);
    }

    public List<String> reportCustomer(List<UUID> tokens) throws Exception {
        if (tokens.size() == 0) {
            return new ArrayList<>();
        }
        List<Triple<String, String, BigDecimal>> l = db.getLog().stream().filter(t ->
                tokens.contains(UUID.fromString(t.first())) || tokens.contains(UUID.fromString(t.second()))
        ).collect(Collectors.toList());
        return convertReportList(l);
    }

    public List<String> reportMerchant(String merchantID) {
        List<Triple<String, String, BigDecimal>> l = db.getLog().stream().filter(t ->
                t.second().equals(merchantID)
        ).collect(Collectors.toList());
        return convertReportList(l);
    }

    public Triple<String, String, BigDecimal> getRefund(UUID token, String user) throws Exception {
        if(db.getRefundedTokens().contains(token.toString())){
            throw new Exception("Already refunded");
        }
        List<Triple<String, String, BigDecimal>> l = db.getLog().stream().filter(t ->
                t.first().equals(token.toString())
        ).collect(Collectors.toList());
        if(l.size() == 0) {
            throw new Exception("Transaction not found");
        } else {
            db.addRefundedToken(token.toString());
            Triple<String, String, BigDecimal> transaction = l.get(0);
            return new Triple<>(transaction.second(), user, transaction.third());
        }
    }
}
