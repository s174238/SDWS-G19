//==========================================
// Author: Michael Guldborg
// Date:   Jan 2021
//==========================================
package org.acme.resources;

import org.acme.businesslogic.ReportService;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/report")
public class ReportResource {
    @GET
    public Response getAllReports() {
        ReportService service = ReportService.getInstance();
        return service.getAllReports();
    }

    @Path("customer/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @GET
    public Response getCustomerReport(@PathParam String userId) {
        ReportService service = ReportService.getInstance();
        return service.getCustomerReports(userId);
    }

    @Path("merchant/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @GET
    public Response getMerchantReport(@PathParam String userId) {
        ReportService service = ReportService.getInstance();
        return service.getMerchantReports(userId);
    }
}
