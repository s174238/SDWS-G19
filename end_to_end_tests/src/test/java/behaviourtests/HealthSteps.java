//==========================================
// Author: Christian
// Date:   Jan 2021
//==========================================
package behaviourtests;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.net.http.HttpResponse;

public class HealthSteps {

    HttpResponse<String> response;

    @When("the client checks the health of the api")
    public void theClientChecksTheHealthOfTheApi() {
        var client = new RestClient();
        response = client.get("health");
    }

    @Then("the api returns the text {string}")
    public void theApiReturnsTheText(String arg0) {
        Assert.assertEquals(200, response.statusCode());
        Assert.assertEquals(arg0, response.body());
    }
}
