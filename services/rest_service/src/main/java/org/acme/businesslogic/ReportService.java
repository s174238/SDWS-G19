//==========================================
// Author: Michael Guldborg
// Date:   Jan 2021
//==========================================
package org.acme.businesslogic;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;

public class ReportService implements EventReceiver {
    private static ReportService _instance;

    public static ReportService getInstance() {
        if (_instance != null) {
            return _instance;
        }

        _instance = new ReportService(new RabbitMqSender());
        RabbitMqListener listener = new RabbitMqListener(_instance);
        try {
            listener.listen();
        } catch (Exception ignored) {
        }
        return _instance;
    }


    private EventSender sender;
    private CompletableFuture<Response> result;

    public ReportService(EventSender sender) {
        this.sender = sender;
    }

    public Response getAllReports() {
        Event getAllReports = new Event("ReportAll");
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(getAllReports);
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    public Response getCustomerReports(String userId) {
        Event getCustomerReports = new Event("ReportCustomer", new Object[]{userId});
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(getCustomerReports);
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    public Response getMerchantReports(String userId) {
        Event getMerchantReports = new Event("ReportMerchant", new Object[]{userId});
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(getMerchantReports);
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }


    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("ReportAllResponse")) {
            System.out.println("event handled: " + event);
            result.complete(Response.ok().entity(event.getArguments()[0]).type(MediaType.APPLICATION_JSON).build());
        } else if (event.getEventType().equals("ReportCustomerResponse")) {
            System.out.println("event handled: " + event);
            result.complete(Response.ok().entity(event.getArguments()[0]).type(MediaType.APPLICATION_JSON).build());
        } else if (event.getEventType().equals("ReportMerchantResponse")) {
            System.out.println("event handled: " + event);
            result.complete(Response.ok().entity(event.getArguments()[0]).type(MediaType.APPLICATION_JSON).build());
        } else if (event.getEventType().equals("ReportCustomerError")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity(event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        }
    }
}
