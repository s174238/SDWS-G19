# Sebastian Tønne Kotewitz s174270

Feature: Confirm payment

  Scenario: Payment is confirmed
    Given a user exists
    And the user has 3 tokens
    And a merchant exists
    When I receive event "PaymentRequested" with user token 1, merchant id, amount 500.00 and description "Hello"
    Then I have sent event "RequestValidated" with the customer id, the merchant id, the amount 500.00 and description "Hello"
    And the database invalid token list of the user includes the invalidated token
    And the database user token list of the user does not include the invalidated token
    And the user now has 2 tokens
    And the user now has 1 invalid tokens

  Scenario: Payment is not confirmed (old token)
    Given a user exists
    And the user has 2 tokens
    And the user has 2 invalid tokens
    And a merchant exists
    When I receive event "PaymentRequested" with invalid token 1, merchant id, amount 500.00 and description "Hello"
    Then I have sent event "ValidationError" with the message "Invalid token"
    And the user now has 2 tokens
    And the user now has 2 invalid tokens


  Scenario: Payment is not confirmed (random token)
    Given a user exists
    And the user has 1 tokens
    And a merchant exists
    When I receive event "PaymentRequested" with a random token, merchant id and amount 500.00
    Then I have sent event "ValidationError" with the message "Invalid token"
    And the user now has 1 tokens
    And the user now has 0 invalid tokens


  Scenario: Payment is not confirmed because of no tokens
    Given a user exists
    And the user has 0 tokens
    And a merchant exists
    When I receive event "PaymentRequested" with a random token, merchant id and amount 500.00
    Then I have sent event "ValidationError" with the message "Invalid token"
    And the user now has 0 tokens
    And the user now has 0 invalid tokens

  Scenario: Account manager fails validation
    When I receive event "RequestValidatedError"
    Then I have sent event "ValidationError" with the message ""