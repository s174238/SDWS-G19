//==========================================
// Author: Michael Guldborg
// Date:   Jan 2021
//==========================================

package org.acme.resources;

import org.acme.businesslogic.TestService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/health")
@Consumes(MediaType.TEXT_PLAIN)
@Produces(MediaType.TEXT_PLAIN)
public class HealthResource {
    @GET
    public Response health() {
        return Response.ok("healthy").build();
    }

    @GET
    @Path("rabbitmq")
    public Response helloWorld() {
        TestService service = TestService.getInstance();
        return service.testRabbitMQ();
    }
}
