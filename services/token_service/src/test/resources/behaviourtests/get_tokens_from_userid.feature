# Sebastian Tønne Kotewitz s174270

Feature: Get tokens from UserID

  Scenario: Get 5 tokens when no tokens are present
    Given a user exists
    And the user has 0 tokens
    When I receive event "GiveTokensUserID" with number 5 and the user's id
    Then I have sent event "GiveTokensUserIDResponse" with the 5 tokens and the user's ID
    And the user now has 5 tokens

  Scenario: Get -1 tokens when no tokens are present
    Given a user exists
    And the user has 0 tokens
    When I receive event "GiveTokensUserID" with number -1 and the user's id
    Then I have sent event "GiveTokensUserIDError" with the message "You must request at least 1 but max 5 tokens"
    And the user now has 0 tokens

  Scenario: Get 5 tokens when user is not present
    When I receive event "GiveTokensUserID" with a random token and the number 5
    Then I have sent event "GiveTokensUserIDResponse" with the 5 tokens and the user's ID

  Scenario: Get 6 tokens when no tokens are present
    Given a user exists
    And the user has 0 tokens
    When I receive event "GiveTokensUserID" with number 6 and the user's id
    Then I have sent event "GiveTokensUserIDError" with the message "Can only request max 5 tokens at once"
    And the user now has 0 tokens

  Scenario: Get 3 tokens when 3 tokens already exists
    Given a user exists
    And the user has 3 tokens
    When I receive event "GiveTokensUserID" with number 3 and the user's id
    Then I have sent event "GiveTokensUserIDError" with the message "Can only request tokens when one or zero tokens remain"
    And the user now has 3 tokens

  Scenario: Receive event with missing arguments
    When I receive event "GiveTokensUserID"
    Then I have sent event "GiveTokensUserIDError" with the message "Invalid or missing arguments"