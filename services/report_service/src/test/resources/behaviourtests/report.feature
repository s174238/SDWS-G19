# Sebastian Tønne Kotewitz s174270

Feature: Report transactions

  Scenario: Report all transactions
    Given no transactions exist
    And a customer token exists
    And a merchant exists
    And a transaction between token and merchant with amount 400.00 exists
    And a customer token exists
    And a transaction between token and merchant with amount 300.00 exists
    When I receive an event "ReportAll"
    Then I have sent an event "ReportAllResponse" with all transactions

  Scenario: Report customer transactions
    Given no transactions exist
    And a customer exists
    And a customer token exists
    And a merchant exists
    And a transaction between token and merchant with amount 123.00 exists
    And a customer token exists
    And a merchant exists
    And a transaction between token and merchant with amount 250.00 exists
    And a different customer exists
    And a transaction between the other customer and merchant with amount 500.00 exists
    When I receive an event "ReportCustomerTokens" with all of the customer's tokens
    Then I have sent an event "ReportCustomerResponse"
    And the sent transactions includes a transaction of "123"
    And the sent transactions includes a transaction of "250"
    And the sent transactions does not include a transaction of "500"

  Scenario: Report customer transactions with refunds
    Given no transactions exist
    And a customer exists
    And a customer token exists
    And a merchant exists
    And a transaction between token and merchant with amount 58.00 exists
    And a refund from merchant to token with amount 58.00 exists
    And a different customer exists
    And a transaction between the other customer and merchant with amount 23.00 exists
    When I receive an event "ReportCustomerTokens" with all of the customer's tokens
    Then I have sent an event "ReportCustomerResponse"
    And the sent transactions includes a transaction of "58"
    And the sent transactions includes a refunded transaction of "58"
    And the sent transactions does not include a transaction of "23"

  Scenario: Report customer transactions empty list
    Given no transactions exist
    And a customer exists
    And a customer token exists
    When I receive an event "ReportCustomerTokens" with all of the customer's tokens
    Then I have sent an event "ReportCustomerResponse"
    And the sent transactions list is empty

  Scenario: Report customer transactions no tokens
    Given no transactions exist
    And a customer exists
    When I receive an event "ReportCustomerTokens" with all of the customer's tokens
    Then I have sent an event "ReportCustomerResponse"
    And the sent transactions list is empty

  Scenario: Report customer when no tokens are sent
    Given a customer exists
    When I receive an event "ReportCustomerTokens" with all of the customer's tokens
    Then I have sent an event "ReportCustomerResponse"
    And the sent transactions list is empty

  Scenario: Report merchant transactions
    Given no transactions exist
    And a merchant exists
    And a customer token exists
    And a merchant exists
    And a transaction between token and merchant with amount 333.00 exists
    And a customer token exists
    And a transaction between token and merchant with amount 444.00 exists
    And a different merchant exists
    And a transaction between token and other merchant with amount 2500.00 exists
    When I receive an event "ReportMerchant" with the merchant id
    Then I have sent an event "ReportMerchantResponse"
    And the sent transactions includes a transaction of "333"
    And the sent transactions includes a transaction of "444"
    And the sent transactions does not include a transaction of "2500"

  Scenario: Report merchant transactions with no id
    When I receive an event "ReportMerchant"
    Then I have sent an event "ReportMerchantError" with the message "Invalid or missing arguments"
