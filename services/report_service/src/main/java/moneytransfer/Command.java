package moneytransfer;

import messaging.Event;
import messaging.EventSender;

public interface Command {
    void execute(Event event, EventSender sender) throws Exception;
}
