/*
 * @author Sebastian Kotewitz s174270
 */

package behaviourtests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.EventSender;
import moneytransfer.DataBank;
import moneytransfer.TokenService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class TokenServiceSteps {
    TokenService s;
    Event event;
    String user, merchant;
    List<UUID> tokens = new ArrayList<>();
    List<UUID> invalidTokens = new ArrayList<>();
    DataBank dataBank = DataBank.getInstance();
    UUID tokenToInvalidate;

    public TokenServiceSteps() {
        s = new TokenService(new EventSender() {

            @Override
            public void sendEvent(Event ev) throws Exception {
                event = ev;
            }

        });
    }

    @When("I receive event {string}")
    public void iReceiveEvent(String string) throws Exception {
        s.receiveEvent(new Event(string));
    }

    @Then("I have sent event {string}")
    public void iHaveSentEvent(String string) {
        assertEquals(string, event.getEventType());
    }

    @Then("I have sent event {string} with the {int} tokens and the user's ID")
    public void iHaveSentEventWithTheGeneratedTokens(String arg0, int arg1) {
        assertEquals(arg0, event.getEventType());
        assertEquals(user, event.getArguments()[0]);
        assertEquals(arg1, ((List) event.getArguments()[1]).size());
    }

    @Then("I have sent event {string} with the message {string}")
    public void iHaveSentEventWithTheMessage(String arg0, String arg1) {
        System.out.println(dataBank.getUsersTokens(user));
        assertEquals(arg0, event.getEventType());
        assertEquals(arg1, event.getArguments()[0]);
    }

    @Given("a user exists")
    public void aUserExists() {
        user = UUID.randomUUID().toString();

    }

    @When("I receive event {string} with number {int} and the user's id")
    public void iReceiveEventWithNumberAndTheUserSId(String arg0, int arg1) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{user.toString(), (double) arg1}));
    }


    @And("the user has {int} tokens")
    public void theUserHasTokens(int arg0) {
        for (int i = 0; i < arg0; i++) {
            tokens.add(UUID.randomUUID());
        }
        dataBank.setUsersTokens(user, tokens);
    }

    @When("I receive event {string} with the token and the number {int}")
    public void iReceiveEventWithTheTokenAndTheNumber(String arg0, int arg1) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{tokens.get(0).toString(), (double) arg1}));
    }

    @Given("a token exists")
    public void aTokenExists() {
        tokens.clear();
        tokens.add(UUID.randomUUID());
    }

    @When("I receive event {string} with the token to invalidate")
    public void iReceiveEventWithTheTokenToInvalidate(String arg0) throws Exception {
        tokenToInvalidate = tokens.get(0);
        s.receiveEvent(new Event(arg0, new Object[]{tokens.get(0).toString()}));
    }

    @Then("I have sent event {string} with the invalidated token")
    public void iHaveSentEventWithTheInvalidatedToken(String arg0) {
        assertEquals(arg0, event.getEventType());
        assertEquals(tokenToInvalidate, event.getArguments()[0]);
    }

    @When("I receive event {string} with a random token")
    public void iReceiveEventWithARandomTokenToInvalidate(String arg0) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{UUID.randomUUID().toString()}));
    }

    @When("I receive event {string} with the token to check")
    public void iReceiveEventWithTheTokenToCheck(String arg0) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{tokens.get(0).toString()}));
    }

    @Then("I have sent event {string} with the valid token")
    public void iHaveSentEventWithTheValidToken(String arg0) {
        assertEquals(arg0, event.getEventType());
        assertEquals(tokens.get(0), event.getArguments()[0]);
    }

    @When("I receive event {string} with the customer's id")
    public void iReceiveEventWithTheCustomerSId(String arg0) throws Exception {
        String userId = "";
        try {
            userId = user.toString();
        } catch (Exception ignored) {
        }
        s.receiveEvent(new Event(arg0, new Object[]{userId}));
    }

    @And("the user has {int} invalid tokens")
    public void theUserHasInvalidTokens(int arg0) {
        List<UUID> tempTokens = dataBank.getUsersTokens(user);
        List<UUID> t = new ArrayList<>();
        for (int i = 0; i < arg0; i++) {
            t.clear();
            UUID token = UUID.randomUUID();
            t.add(token);
            dataBank.setUsersTokens(user, t);
            dataBank.invalidateToken(user, t.get(0));
            invalidTokens.add(token);
        }
        dataBank.setUsersTokens(user, tempTokens);
    }

    @Then("I have sent event {string} with the customer's tokens")
    public void iHaveSentEventWithTheCustomerSTokens(String arg0) {
        assertEquals(arg0, event.getEventType());
        List<UUID> allTokens = new ArrayList<>();
        allTokens.addAll(tokens);
        allTokens.addAll(invalidTokens);
        assertEquals(allTokens, event.getArguments()[0]);
    }

    @And("a merchant exists")
    public void aMerchantExists() {
        merchant = UUID.randomUUID().toString();
    }

    @When("I receive event {string} with user token {int}, merchant id and amount {double}")
    public void iReceiveEventWithUserTokenMerchantIdAndAmount(String arg0, int arg1, double arg2) throws Exception {
        tokenToInvalidate = tokens.get(arg1 - 1);
        s.receiveEvent(new Event(arg0, new Object[]{tokens.get(arg1 - 1).toString(), merchant, arg2}));
    }

    @When("I receive event {string} with a random token, merchant id and amount {double}")
    public void iReceiveEventWithARandomTokenMerchantIdAndAmount(String arg0, double arg1) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{UUID.randomUUID().toString(), merchant, arg1, ""}));
    }

    @Then("I have not sent an event")
    public void iHaveNotSentAnEvent() {
        assertNull(event);
    }

    @When("I receive event {string} with a random token and the number {int}")
    public void iReceiveEventWithARandomTokenAndTheNumber(String arg0, int arg1) throws Exception {
        user = UUID.randomUUID().toString();
        s.receiveEvent(new Event(arg0, new Object[]{user.toString(), (double) arg1}));
    }

    @And("the database invalid token list of the user includes the invalidated token")
    public void theDatabaseInvalidTokenListOfTheUserIncludesTheToken() {
        assertTrue(dataBank.getInvalidTokens(user).contains(tokenToInvalidate));
    }

    @And("the database user token list of the user does not include the invalidated token")
    public void theDatabaseUserTokenListOfTheUserDoesNotIncludeTheToken() {
        assertFalse(dataBank.getUsersTokens(user).contains(tokenToInvalidate));
    }

    @And("the user now has {int} tokens")
    public void theUserNowHasTokens(int arg0) {
        assertEquals(arg0, dataBank.getUsersTokens(user).size());
    }

    @And("the user now has {int} invalid tokens")
    public void theUserNowHasInvalidTokens(int arg0) {
        assertEquals(arg0, dataBank.getInvalidTokens(user).size());
    }

    @When("I receive event {string} with invalid token {int}, merchant id, amount {double} and description {string}")
    public void iReceiveEventWithInvalidTokenMerchantIdAndAmount(String arg0, int arg1, double arg2, String arg3) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{invalidTokens.get(arg1 - 1).toString(), merchant, arg2, arg3}));
    }

    @When("I receive event {string} with user token {int}, merchant id, amount {double} and description {string}")
    public void iReceiveEventWithUserTokenMerchantIdAmountAndDescription(String arg0, int arg1, double arg2, String arg3) throws Exception {
        tokenToInvalidate = tokens.get(arg1 - 1);
        s.receiveEvent(new Event(arg0, new Object[]{tokens.get(arg1 - 1).toString(), merchant, arg2, arg3}));
    }

    @Then("I have sent event {string} with the customer id, the merchant id, the amount {double} and description {string}")
    public void iHaveSentEventWithTheCustomerIdTheMerchantIdTheAmountAndDescription(String arg0, double arg1, String arg2) {
        assertEquals(arg0, event.getEventType());
        assertEquals(user, event.getArguments()[0]);
        assertEquals(merchant, event.getArguments()[1]);
        assertEquals(BigDecimal.valueOf(arg1), event.getArguments()[2]);
        assertEquals(arg2, event.getArguments()[3]);
        assertEquals(tokenToInvalidate, event.getArguments()[4]);
    }

    @Then("I have sent event {string} with an empty list of tokens")
    public void iHaveSentEventWithAnEmptyListOfTokens(String arg0) {
        assertEquals(arg0, event.getEventType());
        assertEquals(new ArrayList<>(), event.getArguments()[0]);
    }
}

