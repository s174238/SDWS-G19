#Author: s174286 Simon S. Brandt
Feature: Account Management

Scenario: Customer Account creation
	Given the customer has a bank account 
	And they are not registered with DTUPay
	When I receive create event "CreateAccount" for that person
	Then I have created the DTUPay account
	Then I have sent the event "AccountCreated"

Scenario: Merchant Account creation
	Given the merchant has a bank account 
	And they are not registered with DTUPay
	When I receive create event "CreateAccount" for that person
	Then I have created the DTUPay account
	Then I have sent the event "AccountCreated"

Scenario: Customer account deletion
	Given the customer has a bank account 
	And they are registered with DTUPay
	When I receive delete event "DeleteAccount" for that person
	Then I have deleted the DTUPay account
	Then I have sent the event "AccountDeleted"

Scenario: Merchant account deletion
	Given the merchant has a bank account 
	And they are registered with DTUPay
	When I receive delete event "DeleteAccount" for that person
	Then I have deleted the DTUPay account
	Then I have sent the event "AccountDeleted"

Scenario: Account creation fails
	Given the customer/merchant has a bank account
	And they are registered with DTUPay
	When I receive create event "CreateAccount" for that person
	Then I have sent the event "CreateAccountError"

Scenario: Account creation fails due to bad argument
	Given the customer/merchant has a bank account
	And they are not registered with DTUPay
	When I receive create event "CreateAccount" with argument null
	Then I have sent the event "CreateAccountError"

Scenario: Account deletion fails
	Given the customer/merchant has a bank account
	And they are not registered with DTUPay
	When I receive delete event "DeleteAccount" for that person
	Then I have sent the event "DeleteAccountError"
	
Scenario: User requests tokens
	Given the customer has a bank account
	And they are registered with DTUPay
	When I receive event "TokensUserID" for that person with amount 5
	Then I have sent the event "GiveTokensUserID" with their ID and amount 5

Scenario: Non-registered user or merchant requests tokens
	Given the customer has a bank account
	And they are not registered with DTUPay as a customer
	When I receive event "TokensUserID" for that person with amount 5
	Then I have sent the event "TokensUserIDError"

Scenario: Payment is initiated
	Given the customer and merchant has a bank account
	And they are registered with DTUPay
	When I receive the payment event "RequestValidated" with UUIDs
	Then I have sent the payment event "RequestValidatedConverted" with AccountIDs

Scenario: Payment initiation fails
	Given the customer and merchant has a bank account
	And they are not registered with DTUPay
	When I receive the payment event "RequestValidated" with UUIDs
	Then I have sent the event "RequestValidatedError"


Scenario: Refund is initiated
	Given the customer and merchant has a bank account
	And they are registered with DTUPay
	When I receive the refund event "RefundRequestTransaction" with UUIDs
	Then I have sent the refund event "RefundRequestTransactionConverted" with AccountIDs

Scenario: Refund initiation fails
	Given the customer and merchant has a bank account
	And they are not registered with DTUPay
	When I receive the refund event "RefundRequestTransaction" with UUIDs
	Then I have sent the event "RefundRequestTransactionError"
	