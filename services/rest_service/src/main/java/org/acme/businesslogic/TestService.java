//==========================================
// Author: Michael Guldborg
// Date:   Jan 2021
//==========================================
package org.acme.businesslogic;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;

public class TestService implements EventReceiver {
    private static final String RabbitMQEvent = "testRabbitMQ";

    private static TestService _instance;

    public static TestService getInstance() {
        if (_instance != null) {
            return _instance;
        }

        _instance = new TestService(new RabbitMqSender());
        RabbitMqListener listener = new RabbitMqListener(_instance);
        try {
            listener.listen();
        } catch (Exception ignored) {
        }
        return _instance;
    }


    private final EventSender sender;
    private CompletableFuture<Response> result;

    public TestService(EventSender sender) {
        this.sender = sender;
    }


    public Response testRabbitMQ() {
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(new Event(RabbitMQEvent));
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e).type(MediaType.TEXT_PLAIN).build();
        }
    }


    @Override
    public void receiveEvent(Event event) {
        if (event.getEventType().equals(RabbitMQEvent)) {
            result.complete(Response.ok("healthy").build());
        }
    }
}
