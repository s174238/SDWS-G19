/*
 * @author Sebastian Kotewitz s174270
 */

package moneytransfer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class DataBank {
    HashMap<String, List<UUID>> userTokens = new HashMap<>();
    HashMap<String, List<UUID>> invalidTokens = new HashMap<>();
    HashMap<UUID, String> tokenToUser = new HashMap<>();

    private DataBank() {
    }

    private static class DataBankHolder {
        static final DataBank instance = new DataBank();
    }

    public static DataBank getInstance() {
        return DataBankHolder.instance;
    }

    public List<UUID> getUsersTokens(String user) {
        return userTokens.get(user);
    }

    public List<UUID> getInvalidTokens(String user) {
        return invalidTokens.get(user);
    }

    public void setUsersTokens(String user, List<UUID> tokens) {
        if(tokens == null){
            return;
        }
        userTokens.put(user, tokens);
        tokens.forEach(t -> {
            tokenToUser.put(t, user);
        });
        invalidTokens.computeIfAbsent(user, k -> new ArrayList<>());
    }

    public String getUserFromToken(UUID token) {
        String user = tokenToUser.get(token);
        List<UUID> activeTokens = userTokens.get(user);
        if (activeTokens != null) {
            if (activeTokens.contains(token)) {
                return user;
            }
        }
        return null;
    }

    public boolean invalidateToken(String user, UUID token) {
        boolean found;
        List<UUID> tokens = userTokens.get(user);
        if (tokens != null) {
            found = tokens.remove(token);
            if (found) {
                List<UUID> userInvalidTokens = invalidTokens.get(user);
                if (userInvalidTokens == null) {
                    userInvalidTokens = new ArrayList<>();
                }
                userInvalidTokens.add(token);
                invalidTokens.put(user, userInvalidTokens);
            }
        } else {
            found = false;
        }
        return found;
    }
}
