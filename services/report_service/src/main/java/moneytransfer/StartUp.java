package moneytransfer;

import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

public class StartUp {
    public static void main(String[] args) throws Exception {
        EventSender sender = new RabbitMqSender();
        ReportService service = new ReportService(sender);
        new RabbitMqListener(service).listen();
    }
}
