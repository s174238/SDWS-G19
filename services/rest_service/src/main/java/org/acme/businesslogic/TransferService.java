//==========================================
// Author: Betta
// Date:   Jan 2021
//==========================================
package org.acme.businesslogic;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
import org.acme.models.TransferRequest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;

public class TransferService implements EventReceiver {
    private static TransferService _instance;

    public static TransferService getInstance() {
        if (_instance != null) {
            return _instance;
        }

        _instance = new TransferService(new RabbitMqSender());
        RabbitMqListener listener = new RabbitMqListener(_instance);
        try {
            listener.listen();
        } catch (Exception ignored) {
        }
        return _instance;
    }


    private EventSender sender;
    private CompletableFuture<Response> result;

    public TransferService(EventSender sender) {
        this.sender = sender;
    }

    public Response transferPayment(TransferRequest request) {
        Event transferPayment = new Event("PaymentRequested", new Object[]{request.token, request.mid, request.amount, request.desc});
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(transferPayment);
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("MoneyTransferred")) {
            System.out.println("event handled: " + event);
            result.complete(Response.ok().entity("Transaction complete").type(MediaType.TEXT_PLAIN).build());
        } else if (event.getEventType().equals("ValidationError")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity((String) event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        } else if (event.getEventType().equals("MoneyTransferFailed")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity((String) event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        }
    }
}
