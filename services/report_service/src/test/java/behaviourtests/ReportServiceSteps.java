/*
 * @author Sebastian Kotewitz s174270
 */

package behaviourtests;

import moneytransfer.ReportServiceFunctions;
import org.glassfish.pfl.basic.contain.Triple;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.EventSender;
import moneytransfer.DataBank;
import moneytransfer.ReportService;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.*;

public class ReportServiceSteps {
    ReportService s;
    Event event;
    UUID token, differentToken;
    String merchant, customer, differentMerchant;
    List<UUID> tokens = new ArrayList<>();
    DataBank db = DataBank.getInstance();
    List<Triple<String, String, BigDecimal>> transactions = new ArrayList<>();
    HashMap<String, List<Triple<String, String, BigDecimal>>> customerTransactions = new HashMap<>();
    HashMap<String, List<Triple<String, String, BigDecimal>>> merchantTransactions = new HashMap<>();
    ReportServiceFunctions rsf = new ReportServiceFunctions();
    Triple<String, String, BigDecimal> transaction;

    public ReportServiceSteps() {
        s = new ReportService(ev -> event = ev);
    }

    @Given("a customer token exists")
    public void aUserExists() {
        token = UUID.randomUUID();
        tokens.add(token);
    }

    @Given("a merchant exists")
    public void aMerchantExists() {
        merchant = UUID.randomUUID().toString();
    }

    @When("I receive an event {string} with customer and merchant info, and the amount {double}")
    public void iReceiveAnEventWithCustomerAndMerchantInfoAndTheAmount(String arg0, double arg1) throws Exception {
        String tokenString = null;
        if(token != null){
            tokenString = token.toString();
        }
        s.receiveEvent(new Event(arg0, new Object[]{tokenString, merchant, arg1}));
    }

    @Then("The log contains a transaction from customer to merchant with amount {double}")
    public void theLogContainsATransactionFromCustomerToMerchantWithAmount(double arg0) {
        assertTrue(db.getLog().contains(new Triple<>(token.toString(), merchant, BigDecimal.valueOf(arg0))));
    }

    @Then("I have sent an event {string}")
    public void iHaveSentAnEvent(String arg0) {
        assertEquals(arg0, event.getEventType());
    }

    @Then("I have sent an event {string} with the message {string}")
    public void iHaveSentAnEventWithTheMessage(String arg0, String arg1) {
        assertEquals(arg0, event.getEventType());
        assertEquals(arg1, event.getArguments()[0]);
    }

    @And("a transaction between token and merchant with amount {double} exists")
    public void aTransactionBetweenTokenAndMerchantWithAmountExists(double arg0) {
        transaction = new Triple<>(token.toString(), merchant, BigDecimal.valueOf(arg0));
        db.addTransaction(token.toString(), merchant, BigDecimal.valueOf(arg0));
        transactions.add(new Triple<>(token.toString(), merchant, BigDecimal.valueOf(arg0)));
        if (customer != null) {
            List<Triple<String, String, BigDecimal>> list = customerTransactions.get(customer);
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(new Triple<>(token.toString(), merchant, BigDecimal.valueOf(arg0)));
            customerTransactions.put(customer, list);
        }
        if (merchant != null) {
            List<Triple<String, String, BigDecimal>> list = merchantTransactions.get(merchant);
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(new Triple<>(token.toString(), merchant, BigDecimal.valueOf(arg0)));
            customerTransactions.put(merchant, list);
        }
    }

    @When("I receive an event {string}")
    public void iReceiveAnEvent(String arg0) throws Exception {
        s.receiveEvent(new Event(arg0));
    }

    @Then("I have sent an event {string} with all transactions")
    public void iHaveSentAnEventWithAllTransactions(String arg0) {
        assertEquals(arg0, event.getEventType());
        assertArrayEquals(rsf.convertReportList(db.getLog()).toArray(), (Object[]) event.getArguments()[0]);
    }

    @And("a customer exists")
    public void aCustomerOwningThisTokenExists() {
        customer = UUID.randomUUID().toString();
    }

    @When("I receive an event {string} with the customer id")
    public void iReceiveAnEventWithTheCustomerId(String arg0) throws Exception {
        String customerId = "";
        if(customer != null){
            customerId = customer;
        }
        s.receiveEvent(new Event(arg0, new Object[]{customerId}));
    }

    @Then("I have sent an event {string} with the customer id")
    public void iHaveSentAnEventWithTheCustomerId(String arg0) {
        assertEquals(arg0, event.getEventType());
        assertEquals(customer, event.getArguments()[0]);
    }

    @When("I receive an event {string} with all of the customer's tokens")
    public void iReceiveAnEventWithAllOfTheCustomerSTokens(String arg0) throws Exception {
        List<String> tokenList = new ArrayList<>();
        tokens.forEach(t->{
            tokenList.add(t.toString());
        });
        s.receiveEvent(new Event(arg0, new Object[]{tokenList}));
    }

    @And("a different customer exists")
    public void aDifferentCustomerExists() {
        differentToken = UUID.randomUUID();
    }

    @And("a transaction between the other customer and merchant with amount {double} exists")
    public void aTransactionBetweenTheOtherCustomerAndMerchantWithAmountExists(double arg0) {
        db.addTransaction(differentToken.toString(), merchant, BigDecimal.valueOf(arg0));
    }

    @When("I receive an event {string} with the message {string}")
    public void iReceiveAnEventWithTheMessage(String arg0, String arg1) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{arg1}));
    }

    @And("a different merchant exists")
    public void aDifferentMerchantExists() {
        differentMerchant = UUID.randomUUID().toString();
    }

    @And("a transaction between token and other merchant with amount {double} exists")
    public void aTransactionBetweenTokenAndOtherMerchantWithAmountExists(double arg0) {
        db.addTransaction(token.toString(), differentMerchant, BigDecimal.valueOf(arg0));
    }

    @When("I receive an event {string} with the merchant id")
    public void iReceiveAnEventWithTheMerchantId(String arg0) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{merchant}));
    }

    @And("the sent transactions includes a transaction of {string}")
    public void theSentTransactionsIncludesATransactionOf(String arg0) {
        Object[] o = (Object[]) event.getArguments()[0];
        String[] s = Arrays.copyOf(o, o.length, String[].class);
        List<String> list = new ArrayList<>(Arrays.asList(s));
        assertTrue(list.stream().filter(t -> t.contains(arg0)).toArray().length >= 1);
    }

    @And("the sent transactions does not include a transaction of {string}")
    public void theCustomerSTransactionsDoesNotIncludeATransactionOf(String arg0) {
        Object[] o = (Object[]) event.getArguments()[0];
        String[] s = Arrays.copyOf(o, o.length, String[].class);
        List<String> list = new ArrayList<>(Arrays.asList(s));
        assertEquals(0, list.stream().filter(t -> t.contains(arg0)).toArray().length);
    }

    @When("I receive an event {string} with no tokens")
    public void iReceiveAnEventWithNoTokens(String arg0) throws Exception {
        s.receiveEvent(new Event(arg0, new Object[]{new String[0]}));
    }

    @Then("I have not sent an event")
    public void iHaveNotSentAnEvent() {
        assertNull(event);
    }

    @And("the sent transactions list is empty")
    public void theSentTransactionsIsEmpty() {
        Object[] o = (Object[]) event.getArguments()[0];
        assertEquals(0, o.length);
    }

    @Given("no transactions exist")
    public void noTransactionsExist() {
        db.clearLog();
    }

    @When("I receive an event {string} with the customer id and the customer token")
    public void iReceiveAnEventWithTheCustomerToken(String arg0) throws Exception {
        s.receiveEvent(new Event(arg0,new Object[]{customer, token.toString()}));
    }

    @Then("I have sent an event {string} with the merchant as debtor, customer as creditor, and amount {double}")
    public void iHaveSentAnEventWithTheMerchantAsDebtorCustomerAsCreditorAndAmount(String arg0, double arg1) {
        assertEquals(arg0, event.getEventType());
        assertEquals(merchant, event.getArguments()[0]);
        assertEquals(customer, event.getArguments()[1]);
        assertEquals(BigDecimal.valueOf(arg1), event.getArguments()[2]);
        assertEquals(token, event.getArguments()[3]);
    }

    @And("the sent transactions includes a refunded transaction of {string}")
    public void theSentTransactionsIncludesARefundedTransactionOf(String arg0) {
        Object[] o = (Object[]) event.getArguments()[0];
        String[] s = Arrays.copyOf(o, o.length, String[].class);
        List<String> list = new ArrayList<>(Arrays.asList(s));
        assertTrue(list.stream().filter(t -> t.contains(arg0)).toArray().length >= 2);
    }

    @And("a refund from merchant to token with amount {double} exists")
    public void aRefundFromMerchantToTokenWithAmountExists(double arg0) {
        db.addTransaction(merchant, token.toString(), BigDecimal.valueOf(arg0));
    }
}

