/*
 * @author Sebastian Kotewitz s174270
 */

package moneytransfer;

import messaging.Event;
import messaging.EventSender;
import org.glassfish.pfl.basic.contain.Triple;

import java.math.BigDecimal;
import java.util.*;

public class ReportServiceCommands {
    private static Map<String, Command> COMMANDS;


    static {
        final Map<String, Command> commands = new HashMap<>();
        ReportServiceFunctions reportServiceFunctions = new ReportServiceFunctions();

        commands.put("LogTransaction", (event, sender) -> {
            String token = (String) event.getArguments()[0];
            String merchant = (String) event.getArguments()[1];
            if (token == null || token.isBlank() || merchant == null || merchant.isBlank()) {
                throw new Exception();
            }
            BigDecimal amount = BigDecimal.valueOf((double) event.getArguments()[2]);
            reportServiceFunctions.addTransactionToLog(token, merchant, amount);
            sender.sendEvent(new Event("LogTransactionResponse"));
        });

        commands.put("ReportAll", (event, sender) -> {
            List<String> result = reportServiceFunctions.reportAll();
            sender.sendEvent(new Event("ReportAllResponse", new Object[]{result.toArray()}));
        });

        commands.put("ReportCustomerTokens", (event, sender) -> {
            List<String> tokensString = (List<String>) event.getArguments()[0];
            List<UUID> tokens = new ArrayList<>();
            tokensString.forEach(t -> {
                tokens.add(UUID.fromString(t));
            });
//            String[] tokensArray = ((String[]) event.getArguments()[0]);
//            for (String s : tokensArray) {
//                tokens.add(UUID.fromString(s));
//            }
            try {
                List<String> cTransactions = reportServiceFunctions.reportCustomer(tokens);
                sender.sendEvent(new Event("ReportCustomerResponse", new Object[]{cTransactions.toArray()}));
            } catch (Exception ex) {
                sender.sendEvent(new Event("ReportCustomerError", new Object[]{ex.getMessage()}));
            }
        });

        commands.put("ReportMerchant", (event, sender) -> {
            String merchantID = (String) event.getArguments()[0];
            List<String> mTransactions = reportServiceFunctions.reportMerchant(merchantID);
            sender.sendEvent(new Event("ReportMerchantResponse", new Object[]{mTransactions.toArray()}));
        });

        commands.put("RefundRequest", (event, sender) -> {
            String userId = (String) event.getArguments()[0];
            UUID token = UUID.fromString((String) event.getArguments()[1]);
            try {
                Triple<String, String, BigDecimal> refund = reportServiceFunctions.getRefund(token, userId);
                sender.sendEvent(new Event(
                        "RefundRequestTransaction",
                        new Object[]{refund.first(), refund.second(), refund.third(), token
                        }));
            } catch (Exception ex) {
                sender.sendEvent(new Event("RefundRequestError", new Object[]{ex.getMessage()}));
            }
        });

        commands.put("RefundRequestTransactionError", (event, sender) -> {
            sender.sendEvent(new Event("RefundRequestError", new Object[]{""}));
        });

        COMMANDS = Collections.unmodifiableMap(commands);
    }

    public void executeCommand(Event event, EventSender sender) throws Exception {
        Command command = COMMANDS.get(event.getEventType());
        if (command != null) {
            command.execute(event, sender);
        }
    }
}
