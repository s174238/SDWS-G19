# Sebastian Tønne Kotewitz s174270

Feature: Refund transaction

  Scenario: Refund transaction
    Given no transactions exist
    And a customer exists
    And a customer token exists
    And a merchant exists
    And a transaction between token and merchant with amount 150.00 exists
    When I receive an event "RefundRequest" with the customer id and the customer token
    Then I have sent an event "RefundRequestTransaction" with the merchant as debtor, customer as creditor, and amount 150.0

  Scenario: Refund already refunded transaction
    Given no transactions exist
    And a customer exists
    And a customer token exists
    And a merchant exists
    And a transaction between token and merchant with amount 350.00 exists
    When I receive an event "RefundRequest" with the customer id and the customer token
    Then I have sent an event "RefundRequestTransaction" with the merchant as debtor, customer as creditor, and amount 350.0
    When I receive an event "RefundRequest" with the customer id and the customer token
    Then I have sent an event "RefundRequestError" with the message "Already refunded"

  Scenario: Refund transaction that does not exist
    Given no transactions exist
    And a customer exists
    And a customer token exists
    When I receive an event "RefundRequest" with the customer id and the customer token
    Then I have sent an event "RefundRequestError" with the message "Transaction not found"
    
  Scenario: Account manager returns a refund transaction error
    When I receive an event "RefundRequestTransactionError"
    Then I have sent an event "RefundRequestError" with the message ""