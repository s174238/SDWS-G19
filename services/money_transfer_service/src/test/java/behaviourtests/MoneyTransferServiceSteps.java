package behaviourtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Consumer;

import dtu.bank.*;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import messaging.Event;
import messaging.EventSender;
import moneytransfer.MoneyTransferService;

/**
*
* @author s174238 Mads Christensen
*/
public class MoneyTransferServiceSteps {
	
	BankService bank;
	
	MoneyTransferService s;
	Event event;
	Event sentEvent;
	ArrayList<Event> events = new ArrayList<Event>();
	
	String eventName;
	
	boolean status;
	
	String cfname;
	String clname;
	String ccpr;
	int cbalance;
	String cid;
	
	String mfname;
	String mlname;
	String mcpr;
	int mbalance;
	String mid;
	
	BigDecimal amount;
	
	String error;
	
	String token = UUID.randomUUID().toString();
	
	public MoneyTransferServiceSteps() {
		bank = new BankServiceService().getBankServicePort();
		s = new MoneyTransferService(new EventSender() {

			@Override
			public void sendEvent(Event ev) throws Exception {
				events.add(ev);
				event = ev;
				if (ev.getEventType().equals("MoneyTransferFailed") || ev.getEventType().equals("RefundTransferFailed")) {
					error = (String) ev.getArguments()[0];
				}
			}
			
		});
		
	}
	
	@Given("the customer {string} {string} with CPR {string} has a bank account")
	public void theCustomerWithCPRHasABankAccount(String string, String string2, String string3) {
	    cfname = string;
	    clname = string2;
	    ccpr = string3;
	}

	@Given("the balance of customer account is {int}")
	public void theBalanceOfCustomerAccountIs(Integer int1) {
	    cbalance = int1;
	}

	@Given("the customer is registered with DTUPay")
	public void theCustomerIsRegisteredWithDTUPay() {
		try {
			User u = new User(); 
			u.setCprNumber(ccpr);
			u.setFirstName(cfname);
			u.setLastName(clname);
			cid = bank.createAccountWithBalance(u, BigDecimal.valueOf(cbalance));
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}
	}
	
	@Given("the customer is not registered with DTUPay")
	public void theCustomerIsNotRegisteredWithDTUPay() {
		Account acc = null; 
		try {
			acc = bank.getAccountByCprNumber(ccpr);
		} catch (BankServiceException_Exception e) {
		}
		cid = "";
		assertTrue(acc == null);
	}
	
	@Given("the merchant {string} {string} with CPR number {string} has a bank account")
	public void theMerchantWithCPRNumberHasABankAccount(String string, String string2, String string3) {
		mfname = string;
	    mlname = string2;
	    mcpr = string3;
	}
	
	@Given("the balance of merchant account is {int}")
	public void theBalanceOfMerchantAccountIs(Integer int1) {
		mbalance = int1;
	}

	@Given("the merchant is registered with DTUPay")
	public void theMerchantIsRegisteredWithDTUPay() {
		try {
	    	User u = new User(); 
	    	u.setCprNumber(mcpr);
	    	u.setFirstName(mfname);
	    	u.setLastName(mlname);
			mid = bank.createAccountWithBalance(u, BigDecimal.valueOf(mbalance));
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}
	}
	
	@Given("the merchant is not registered with DTUPay")
	public void theMerchantIsNotRegisteredWithDTUPay() {
		Account acc = null; 
		try {
			acc = bank.getAccountByCprNumber(mcpr);
		} catch (BankServiceException_Exception e) {
		}
		mid = "";
		assertTrue(acc == null);
	}
	
	@When("I receive event {string} with customer, merchant and amount {string}")
	public void iReceiveEvent(String string, String int1) {
		double i = Double.parseDouble(int1);
		eventName = string;
		amount = BigDecimal.valueOf(i);
		String debtor = string.equals("RefundRequestTransactionConverted") ? mid : cid;
		String creditor = string.equals("RefundRequestTransactionConverted") ? cid : mid;
		
		Object[] objs = {debtor, creditor, i, token, "Refund", mid};
		sentEvent = new Event(eventName, objs);
		try {
			s.receiveEvent(sentEvent);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	
	@Then("transfer succeeds")
	public void transferSucceeds() {
	    assertTrue(error == null);
	    
	}

	@Then("the balance of the customer at the bank is {string} kr")
	public void theBalanceOfTheCustomerAtTheBankIsKr(String int1) {
		try {
			assertEquals(int1, bank.getAccount(cid).getBalance().toString());
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}
	}

	@Then("the balance of the merchant at the bank is {string} kr")
	public void theBalanceOfTheMerchantAtTheBankIsKr(String int1) {
	    try {
			assertEquals(int1, bank.getAccount(mid).getBalance().toString());
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}
	}
	
	@Then("I have sent event {string}")
	public void iHaveSentEvent(String string) {
//		assertEquals(string,event.getEventType());
		boolean containsEvent = false;
		for (Event event : events) {
			if (event.getEventType().equals(string)){
				containsEvent = true;
			}
		}
		assertTrue(containsEvent);
	}
	

	@Then("transfer fails")
	public void transferFails() {
	    assertFalse(error == null);
	}

	@Then("event contains error {string}")
	public void eventContainsError(String string) {
	    assertEquals(string, error);
	}

	@Before()
	public void clearAccounts() {
		try {
			String c = bank.getAccountByCprNumber("++++++-++++c").getId();
			bank.retireAccount(c);
		} catch (BankServiceException_Exception e) {
		}
		try {
			String m = bank.getAccountByCprNumber("******-****1").getId();
			bank.retireAccount(m);
		} catch (BankServiceException_Exception e) {
		}
	}

}

