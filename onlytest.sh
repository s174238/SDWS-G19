#!/bin/bash
#==========================================
# Author: Sebastian
# Date:   Jan 2021
#==========================================

# Run all services
sh run.sh

sleep 20s

sh run.sh

# Run all end to end tests
cd end_to_end_tests && mvn test -e

# Stop all docker images
docker-compose down

# Remove unused images (cleanup)
docker image prune -f

