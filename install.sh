#!/bin/bash
#==========================================
# Author: Betta
# Date:   Jan 2021
#==========================================


# Install libraries
(cd libraries/bank-objects && mvn clean install)
(cd libraries/message-utilities && mvn clean install)

# Install services
(cd services/acc_management_service && mvn clean install)
(cd services/money_transfer_service && mvn clean install)
(cd services/report_service && mvn clean install)
(cd services/rest_service && mvn clean install)
(cd services/token_service && mvn clean install)

# Install end_to_end_tests
(cd end_to_end_tests && mvn clean install)
