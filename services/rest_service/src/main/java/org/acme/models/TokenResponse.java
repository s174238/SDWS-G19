package org.acme.models;

import java.util.List;

public class TokenResponse {
    public String userId;
    public List<String> tokens;

    public TokenResponse(String userId, List<String> tokens) {
        this.userId = userId;
        this.tokens = tokens;
    }
}
