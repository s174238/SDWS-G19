/*
 * @author Sebastian Kotewitz s174270
 */

package moneytransfer;

import org.glassfish.pfl.basic.contain.Triple;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class DataBank {
    List<Triple<String, String, BigDecimal>> log = new ArrayList<>();
    List<String> refundedTokens = new ArrayList<>();

    private DataBank() {
    }

    private static class DataBankHolder {
        static final DataBank instance = new DataBank();
    }

    public static DataBank getInstance() {
        return DataBankHolder.instance;
    }

    public void addRefundedToken(String token){
        refundedTokens.add(token);
    }

    public List<String> getRefundedTokens(){
        return Collections.unmodifiableList(refundedTokens);
    }

    public void addTransaction(String token, String merchant, BigDecimal amount) {
        log.add(new Triple<>(token, merchant, amount));
    }

    public List<Triple<String, String, BigDecimal>> getLog() {
        return Collections.unmodifiableList(log);
    }

    public void clearLog(){
        log.clear();
    }

}
