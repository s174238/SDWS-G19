package org.acme.models;

import java.util.UUID;

public class RefundRequest {
    public String userId;
    public String token;
}
