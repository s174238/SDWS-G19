#!/bin/bash
set -e
mvn clean package
docker-compose build money_transfer_service
