# Sebastian Tønne Kotewitz s174270

Feature: Add transaction to log

  Scenario: Add transaction to log
    Given a customer token exists
    And a merchant exists
    When I receive an event "LogTransaction" with customer and merchant info, and the amount 500.0
    Then I have sent an event "LogTransactionResponse"
    And The log contains a transaction from customer to merchant with amount 500.0

  Scenario: Add transaction to log with no arguments
    When I receive an event "LogTransaction" with customer and merchant info, and the amount 500.0
    Then I have sent an event "LogTransactionError" with the message "Invalid or missing arguments"

  Scenario: Add transaction to log with no merchant
    Given a customer exists
    When I receive an event "LogTransaction" with customer and merchant info, and the amount 500.0
    Then I have sent an event "LogTransactionError" with the message "Invalid or missing arguments"