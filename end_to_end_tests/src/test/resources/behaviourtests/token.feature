#==========================================
# Author: Sebastian Kotewitz
# Date:   Jan 2021
#==========================================
Feature: Token

  Scenario: Costumer requests tokens with 0 unused tokens
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    Then the customer is registered with the Bank Account ID in DTUPay
    When the costumer requests 2 tokens via id
    Then the costumer has 2 unused tokens

  Scenario: Costumer requests tokens with 1 unused tokens
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    Then the customer is registered with the Bank Account ID in DTUPay
    When the costumer requests 1 tokens via id
    Then the costumer has 1 unused tokens
    When the costumer requests 4 tokens via id
    Then the costumer has 5 unused tokens

  Scenario: Costumer requests tokens with more than 1 unused tokens
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    Then the customer is registered with the Bank Account ID in DTUPay
    When the costumer requests 2 tokens via id
    Then the costumer has 2 unused tokens
    When the costumer requests 5 tokens via id
    Then I get an error

  Scenario: Costumer requests too many tokens
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    Then the customer is registered with the Bank Account ID in DTUPay
    When the costumer requests 6 tokens via id
    Then I get an error

  Scenario: Customer requests tokens with token
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    Then the customer is registered with the Bank Account ID in DTUPay
    When the costumer requests 1 tokens via id
    Then the costumer has 1 unused tokens
    When the costumer requests 4 tokens via token
    Then the costumer has 5 unused tokens

  Scenario: Customer requests tokens with token, but already has too many tokens
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    Then the customer is registered with the Bank Account ID in DTUPay
    When the costumer requests 2 tokens via id
    Then the costumer has 2 unused tokens
    When the costumer requests 4 tokens via token
    Then I get an error

  Scenario: Customer requests tokens with invalid token
    When the costumer requests 4 tokens via a random token
    Then I get the error "Invalid token"
