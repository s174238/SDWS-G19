#!/bin/bash

# Build quarkus application
mvn package -Dquarkus.native.container-build=true

# Build docker image from application
docker build -t quarkus/rest_service .

# Run docker image
docker run -i --rm -p 8080:8080 --name rest_service quarkus/rest_service
