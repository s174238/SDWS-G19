//==========================================
// Author: Betta
// Date:   Jan 2021
//==========================================
package org.acme.businesslogic;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
import org.acme.models.TokenFromTokenRequest;
import org.acme.models.TokenRequest;
import org.acme.models.TokenResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class TokenService implements EventReceiver {
    private static TokenService _instance;

    public static TokenService getInstance() {
        if (_instance != null) {
            return _instance;
        }

        _instance = new TokenService(new RabbitMqSender());
        RabbitMqListener listener = new RabbitMqListener(_instance);
        try {
            listener.listen();
        } catch (Exception ignored) {
        }
        return _instance;
    }


    private EventSender sender;
    private CompletableFuture<Response> result;

    public TokenService(EventSender sender) {
        this.sender = sender;
    }

    public Response getTokens(TokenRequest request) {
        Event getTokens = new Event("TokensUserID", new Object[]{request.userId, request.number});
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(getTokens);
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    public Response getTokensFromToken(TokenFromTokenRequest request) {
        Event getTokensFromToken = new Event("GiveTokensToken", new Object[]{request.token, request.number});
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(getTokensFromToken);
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("GiveTokensUserIDResponse")) {
            System.out.println("event handled: " + event);
            TokenResponse response = new TokenResponse((String) event.getArguments()[0], (List<String>) event.getArguments()[1]);
            result.complete(Response.ok(response).type(MediaType.APPLICATION_JSON).build());
        } else if (event.getEventType().equals("TokensUserIDError")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity((String) event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        } else if (event.getEventType().equals("GiveTokensUserIDError")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity((String) event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        } else if (event.getEventType().equals("GiveTokensTokenResponse")) {
            System.out.println("event handled: " + event);
            TokenResponse response = new TokenResponse((String) event.getArguments()[0], (List<String>) event.getArguments()[1]);
            result.complete(Response.ok().entity(response).type(MediaType.APPLICATION_JSON).build());
        } else if (event.getEventType().equals("GiveTokensTokenError")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity((String) event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        }
    }
}
