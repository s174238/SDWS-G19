/*
 * @author Sebastian Kotewitz s174270
 */

package moneytransfer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TokenServiceFunctions {

    final int MAX_TOKENS_REQ = 5;
    DataBank dataBank = DataBank.getInstance();

    public String getUserFromToken(UUID token) {
        return dataBank.getUserFromToken(token);
    }

    public List<UUID> generateTokens(String user, int number) throws Exception {
        if (number > MAX_TOKENS_REQ) {
            throw new Exception("Can only request max " + MAX_TOKENS_REQ + " tokens at once");
        }
        if (number <= 0) {
            throw new Exception("You must request at least 1 but max " + MAX_TOKENS_REQ + " tokens");
        }
        List<UUID> userTokens = dataBank.getUsersTokens(user);
        if (userTokens == null) {
            userTokens = new ArrayList<>();
        }
        if (userTokens.size() > 1) {
            throw new Exception("Can only request tokens when one or zero tokens remain");
        }
        for (int i = 0; i < number; i++) {
            userTokens.add(UUID.randomUUID());
        }
        dataBank.setUsersTokens(user, userTokens);
        return userTokens;
    }

    List<UUID> generateTokenWithToken(UUID token, int number) throws Exception {
        String user = dataBank.getUserFromToken(token);
        if (user == null) {
            throw new Exception("Invalid token");
        }
        return generateTokens(user, number);
    }

    UUID invalidateToken(UUID token) throws Exception {
        String user = getUserFromToken(token);
        if (user == null) {
            throw new Exception("Invalid token");
        }
        dataBank.invalidateToken(user, token);
        return token;
    }

    public List<UUID> getAllUserTokens(String userID) {
        List<UUID> result = new ArrayList<>();
        List<UUID> t1 = dataBank.getUsersTokens(userID);
        List<UUID> t2 = dataBank.getInvalidTokens(userID);
        if (t1 != null) {
            result.addAll(t1);
        }
        if (t2 != null) {
            result.addAll(t2);
        }
        return result;
    }

    public void validatePayment(UUID token) throws Exception {
        String user = getUserFromToken(token);
        if (user == null) {
            throw new Exception("Invalid token");
        }
        invalidateToken(token);
    }
}
