# Sebastian Tønne Kotewitz s174270

Feature: Irrelevant event
  Scenario: Receiving an event which is not registered as a command
    When I receive an event "NOTRELEVANTFORTHISMODULE"
    Then I have not sent an event