//==========================================
// Author: Michael Guldborg
// Date:   Jan 2021
//==========================================
package org.acme.businesslogic;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
import org.acme.models.RefundRequest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;

public class RefundService implements EventReceiver {
    private static RefundService _instance;

    public static RefundService getInstance() {
        if (_instance != null) {
            return _instance;
        }

        _instance = new RefundService(new RabbitMqSender());
        RabbitMqListener listener = new RabbitMqListener(_instance);
        try {
            listener.listen();
        } catch (Exception ignored) {
        }
        return _instance;
    }


    private EventSender sender;
    private CompletableFuture<Response> result;

    public RefundService(EventSender sender) {
        this.sender = sender;
    }

    public Response refundPayment(RefundRequest request) {
        Event refundPayment = new Event("RefundRequest", new Object[]{request.userId, request.token});
        result = new CompletableFuture<>();
        try {
            sender.sendEvent(refundPayment);
            return result.join();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
    }


    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("RefundTransferred")) {
            System.out.println("event handled: " + event);
            result.complete(Response.ok().entity((String) event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        } else if (event.getEventType().equals("RefundRequestError")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity((String) event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        } else if (event.getEventType().equals("RefundTransferFailed")) {
            System.out.println("event handled: " + event);
            result.complete(Response.status(400).entity((String) event.getArguments()[0]).type(MediaType.TEXT_PLAIN).build());
        }
    }
}
