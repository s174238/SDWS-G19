package models;

import java.util.ArrayList;

public class DTUPayUser {
    public String uid;
    public String cpr;
    public boolean isRegistered;
    public int balance;
    public ArrayList<String> tokens = new ArrayList<>();
}
