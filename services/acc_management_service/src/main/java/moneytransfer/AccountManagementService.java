package moneytransfer;

import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import dtu.bank.*;

//Author: s174286 Simon S. Brandt

public class AccountManagementService implements EventReceiver {

	EventSender sender;
	Double amount;

	HashMap<UUID, String> merchantAccounts = new HashMap<UUID, String>();
	HashMap<UUID, String> customerAccounts = new HashMap<UUID, String>();
	
	

	public AccountManagementService(EventSender sender) {
		this.sender = sender;
	}
	
	public HashMap<UUID, String> getCustomerAccounts() {
		return customerAccounts;	
	}
	
	public HashMap<UUID, String> getMerchantAccounts() {
		return merchantAccounts;	
	}
	
	public UUID addAccount(String accID, String type) {
		if (accID != null) {
			UUID uuid = UUID.randomUUID();
			if (type.equals("customer") && !customerAccounts.containsValue(accID)) {
				customerAccounts.put(uuid, accID);
				return uuid;
			}
			if (type.equals("merchant") && !merchantAccounts.containsValue(accID)) {
				merchantAccounts.put(uuid, accID);	
				return uuid;
			}
		}
		return null; 	
	}
	
	public void removeAccount(UUID uuid, String type) {
		if (type.equals("customer")) {
			customerAccounts.remove(uuid);
		}
		if (type.equals("merchant")) {
			merchantAccounts.remove(uuid);	
		}
	}
	
	public boolean accountExists(UUID uuid, String type) {
		if (type.equals("customer") && customerAccounts.containsKey(uuid)) {
			return true;
		}
		if (type.equals("merchant") && merchantAccounts.containsKey(uuid)) {
			return true;
		}
		
		return false;
	}
	
	public void clearAccounts() {
		customerAccounts.clear();	
		merchantAccounts.clear();	
	}
	
	  
    @Override
    public void receiveEvent(Event event) throws Exception {
    	if (event.getEventType().equals("CreateAccount")) { // create account with bank acc
    		String bankAcc = (String) event.getArguments()[0];
    		String type = (String) event.getArguments()[1];
    		UUID id = addAccount(bankAcc, type);

    		System.out.println("BankAccountId: " + bankAcc);
			System.out.println("Type: " + type);
			System.out.println("UserId: " + id);

			Event e;
    		if (id == null) {
				e = new Event(event.getEventType() + "Error", new Object[]{"Account creation: Account already exists"});
			} else {
    			String msg = "Account successfully created";
				e = new Event("AccountCreated", new Object[]{id, msg});
			}

			sender.sendEvent(e);
    	}
    	
    	if (event.getEventType().equals("DeleteAccount")) {  //delete account with UUID, not with bank acc
			UUID id = null;
			try{
				id = UUID.fromString((String) event.getArguments()[0]);
			} catch (Exception e){

			}
			String type = (String) event.getArguments()[1];

    		if (accountExists(id,type)) { 
				removeAccount(id, type);
				Event e = new Event("AccountDeleted", new String[]{"Account successfully deleted"});
				sender.sendEvent(e);
			} else {
				sender.sendEvent(new Event(event.getEventType() + "Error", new String[]{"Account Deleton Error: Invalid/missing arguments or account does not exist"}));
			}    
    	}
    	
    	if (event.getEventType().equals("TokensUserID")) {   
    		UUID cid = null;
    		try {
				cid = UUID.fromString((String) event.getArguments()[0]);
			} catch (Exception e){

			}

    		if (customerAccounts.containsKey(cid)) {
    			Event e = (new Event("GiveTokensUserID", new Object[]{cid, event.getArguments()[1]}));    	
				sender.sendEvent(e);
    		} else {
    			sender.sendEvent(new Event(event.getEventType() + "Error", new String[]{"User does not have a bank account"}));
            }  		            
        }
    	
    	if (event.getEventType().equals("RequestValidated")) {
    		UUID cid = null;
    		UUID mid = null;
    		try {
				cid = UUID.fromString((String) event.getArguments()[0]);
				mid = UUID.fromString((String) event.getArguments()[1]);
			} catch (Exception e){

			}

    		if (customerAccounts.containsKey(cid) && merchantAccounts.containsKey(mid)) {
    			Event e = (new Event("RequestValidatedConverted", new Object[]{customerAccounts.get(cid), merchantAccounts.get(mid), event.getArguments()[2], event.getArguments()[3], event.getArguments()[4], mid}));
				sender.sendEvent(e);
    		} else {
    			sender.sendEvent(new Event(event.getEventType() + "Error", new String[]{"User does not have a bank account"}));
            }  		           
        }
    	
    	if (event.getEventType().equals("RefundRequestTransaction")) {
    		UUID mid = null;
    		UUID cid = null;
    		try{
				mid = UUID.fromString((String) event.getArguments()[0]);
				cid = UUID.fromString((String) event.getArguments()[1]);
			} catch (Exception e){

			}

    		if (customerAccounts.containsKey(cid) && merchantAccounts.containsKey(mid)) {
    			Event e = (new Event("RefundRequestTransactionConverted", new Object[]{merchantAccounts.get(mid), customerAccounts.get(cid), event.getArguments()[2], event.getArguments()[3], mid}));
				sender.sendEvent(e);
    		} else {
    			sender.sendEvent(new Event(event.getEventType() + "Error", new String[]{"User does not have a bank account"}));
            }  		           
        }
    }
}
 
