<!--==========================================
# Author: Michael
# Date:   Jan 2021
#==========================================-->

# SDWS-G19
> This is the source code for the micro service project DTUPay

The root folder contains the following scripts used for development and testing the application

```bash
# The following command builds all microservices
sh build.sh

# The following command runs all microservices in docker
# The swagger doc is then available at http://localhost:8080/swagger-ui
sh run.sh

# The following command will execute build.sh, run.sh and then run end_to_end_tests
sh test.sh
```





### How to install
```
# Install libraries
cd libraries
#cd back-objects && mvn clean install && cd ../
cd messaging-utilities && mvn clean install && cd ../
cd ../

# Install services
cd services
cd token_service && mvn clean install && cd ../
cd rest_service && mvn clean install && cd ../
```

### Build and run quarkus rest_service
> Either use the run.sh script or follow the following manual guide

```bash
# Build quarkus application
mvn package -Dquarkus.native.container-build=true

# Build quarkus docker image
docker build -f src/main/docker/Dockerfile.jvm -t quarkus/rest_service .

# Run quarkus docker image
docker run -i --rm -p 8080:8080 quarkus/rest_service

```