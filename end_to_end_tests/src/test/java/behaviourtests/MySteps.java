//==========================================
// Author: Betta
// Date:   Jan 2021
//==========================================
package behaviourtests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.DTUPayUser;
import org.acme.models.*;
import org.acme.resources.ReportResource;
import org.eclipse.persistence.queries.ReportQuery;
import org.jboss.resteasy.spi.NotImplementedYetException;
import org.junit.After;
import org.junit.Assert;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.concurrent.TimeUnit;

import dtu.bank.*;

public class MySteps {

    BankService bank = new BankServiceService().getBankServicePort();
    public RestClient client = new RestClient();
    public DTUPayUser customer = new DTUPayUser() {{
        this.cpr = "141414-1414";
    }};
    public DTUPayUser merchant = new DTUPayUser() {{
        this.cpr = "151515-1515";
    }};
    String bankId;
    String type;
    String error;

    public HttpResponse<String> response;
    private List<String> transactions = new ArrayList<>();


    @Given("the costumer {string} has {int} unused tokens")
    public void theCostumerHasUnusedTokens(String uid, int count) {
        customer.uid = uid;
        customer.tokens = new ArrayList<>(Collections.nCopies(count, "token"));
    }


    @When("the costumer requests {int} tokens")
    public void theCostumerRequestsTokens(int count) throws IOException {
        response = client.postJson("tokens", new TokenRequest() {{
            this.userId = customer.uid;
            this.number = count;
        }});
        System.out.println(response.statusCode());
        System.out.println(response.body());
        if (response.statusCode() == 200) {
            var tokens = client.<String>mapList(response.body());
            customer.tokens.addAll(tokens);
        }
    }

    @Then("the costumer has {int} unused tokens")
    public void theCostumerHasUnusedTokens(int tokenCount) {
        Assert.assertEquals(200, response.statusCode());
        Assert.assertEquals(customer.tokens.size(), tokenCount);
    }

    @Then("the request is denied")
    public void theRequestIsDenied() {
        Assert.assertNotEquals(200, response.statusCode());
    }


    @Given("the costumer {string}")
    public void theCostumer(String uid) {
        customer.uid = uid;
    }

    @When("the costumer requests a list of his transactions in the time period {string}")
    public void theCostumerRequestsAListOfHisTransactionsInTheTimePeriod(String time) {
        var start = time.split(",")[0];
        var end = time.split(",")[1];
        response = client.get("report/customer" + customer.uid);
    }

    @Then("the costumer gets a list of his transactions for the period")
    public void theCostumerGetsAListOfHisTransactionsForThePeriod() {
    }

    @Given("the merchant {string}")
    public void theMerchant(String uid) {
        merchant.uid = uid;
    }

    @When("the merchant requests a list of his transactions in the time period {string}")
    public void theMerchantRequestsAListOfHisTransactionsInTheTimePeriod(String time) {
        var start = time.split(",")[0];
        var end = time.split(",")[1];
        response = client.get("report/merchant/" + merchant.uid);
    }

    @Then("the merchant gets a list of his transactions for the period")
    public void theMerchantGetsAListOfHisTransactionsForThePeriod() {
    }

    @Given("the customer {string} with CPR {string} has a bank account")
    public void theCustomerWithCPRHasABankAccount(String arg0, String arg1) {
        customer.uid = arg1;
    }

    @And("the balance of that account is {int}")
    public void theBalanceOfThatAccountIs(int arg0) {
    }

    @And("the customer is registered with DTUPay")
    public void theCustomerIsRegisteredWithDTUPay() {
        Assert.assertTrue(customer.isRegistered);
    }

    @And("the merchant {string} with CPR number {string} has a bank account")
    public void theMerchantWithCPRNumberHasABankAccount(String arg0, String arg1) {
        merchant.uid = arg1;
    }

    @And("the merchant is registered with DTUPay")
    public void theMerchantIsRegisteredWithDTUPay() {
        Assert.assertTrue(merchant.isRegistered);
    }

    @When("the costumer presents an unused token to the merchant")
    public void theCostumerPresentsAnUnusedTokenToTheMerchant() {
        throw new NotImplementedYetException();
    }

    @Then("the merchant starts a payment for {int} kr by the customer")
    public void theMerchantStartsAPaymentForKrByTheCustomer(int arg0) {
        throw new NotImplementedYetException();
    }


    @And("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(int arg0) {
        Assert.assertEquals(merchant.balance, arg0);
    }

    @And("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(int arg0) {
        Assert.assertEquals(customer.balance, arg0);
    }


    // REGISTER
    @Given("a new customer has a bank account")
    public void aNewCustomer() {
        try {
            type = "customer";
            User u = new User();
            u.setFirstName("m");
            u.setLastName("l");
            u.setCprNumber(customer.cpr);
            bankId = bank.createAccountWithBalance(u, BigDecimal.valueOf(10000.0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @Given("a new merchant has a bank account")
    public void aNewMerchant() {
        try {
            type = "merchant";
            User u = new User();
            u.setFirstName("c");
            u.setLastName("p");
            u.setCprNumber(merchant.cpr);
            bankId = bank.createAccountWithBalance(u, BigDecimal.valueOf(10000.0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }


    @When("the customer registers with their Bank Account ID as customer")
    public void theCustomerRegisters() {
        RegisterRequest regReq = new RegisterRequest();
        regReq.accountId = bankId;
        regReq.type = type;
        response = client.postJson("account", regReq);

        System.out.println("Error: " + response.body());
        System.out.println("Type of body: " + response.body().getClass());
        RegisterResponse registerResponse = client.mapObject(response.body(), RegisterResponse.class);
        customer.uid = registerResponse.userId;
    }

    @When("the merchant registers with their Bank Account ID as merchant")
    public void theMerchantRegisters() {
        var regReq = new RegisterRequest();
        regReq.accountId = bankId;
        regReq.type = type;
        response = client.postJson("account", regReq);
        RegisterResponse registerResponse = client.mapObject(response.body(), RegisterResponse.class);
        merchant.uid = registerResponse.userId;
    }

    @Then("the customer is registered with the Bank Account ID in DTUPay")
    public void theCustomerIsRegisteredWithTheId() {
        Assert.assertEquals(200, response.statusCode());
        Assert.assertNotNull(customer.uid);
    }

    @Then("the merchant is registered with the Bank Account ID in DTUPay")
    public void theMerchantIsRegisteredWithTheId() {
        Assert.assertEquals(200, response.statusCode());
        Assert.assertNotNull(merchant.uid);
    }

    @Given("a registered {string} with the id {string}")
    public void aRegisteredWithTheId(String arg0, String arg1) throws Exception {
        var theType = arg0;
        User user = new User() {{
            this.firstName = arg1;
            this.lastName = arg1;
            this.cprNumber = arg1;
        }};
        var bankId = bank.createAccountWithBalance(user, BigDecimal.valueOf(0.0));

        var regReq = new RegisterRequest() {{
            this.accountId = bankId;
            this.type = theType;
        }};
        HttpResponse<String> response = client.postJson("account", regReq);
        var registerResponse = client.mapObject(response.body(), RegisterResponse.class);
        customer = new DTUPayUser() {{
            this.cpr = user.getCprNumber();
            this.uid = registerResponse.userId;
        }};
    }

    @Given("the customer has {int} unused tokens")
    public void theCustomerHasUnusedTokens(int arg0) throws Exception {
        var request = new TokenRequest();
        request.userId = customer.uid;
        request.number = arg0;
        response = client.postJson("tokens", request);
        customer.tokens.clear();
        customer.tokens.addAll(client.<String>mapList(response.body()));
    }

    @Before()
    public void clearAccounts() throws Exception {
        forceRetire(customer.cpr);
        forceRetire(merchant.cpr);
        forceRetire("112233-1234");
//        client.delete("account/" + customer.uid);
        //client.delete("account/" + customer.uid);
        //client.delete("account/" + merchant.uid);
    }

    @After()
    public void clearAccounts2() throws Exception {
        forceRetire(customer.cpr);
        forceRetire(merchant.cpr);
        forceRetire("112233-1234");
//        client.delete("account/" + customer.uid);
        //client.delete("account/" + customer.uid);
        //client.delete("account/" + merchant.uid);
    }

    public void forceRetire(String cpr) throws Exception {
        try {
            bank.retireAccount(bank.getAccountByCprNumber(cpr).getId());
        } catch (BankServiceException_Exception e) {
            if (e.getMessage().equals("Account does not exist")) {
                return;
            }
            throw e;
        }
    }

    @When("the costumer requests {int} tokens via id")
    public void theCostumerRequestsTokensViaId(int arg0) {
        TokenRequest tokReq = new TokenRequest();
        tokReq.userId = customer.uid;
        tokReq.number = arg0;
        response = client.postJson("tokens", tokReq);
        if (response.statusCode() == 200) {
            Map<String, Object> m = objectMap(response.body());
            customer.tokens = (ArrayList<String>) m.get("tokens");
        }
    }

    Map<String, Object> objectMap(String json) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> m;
        try {
            m = mapper.readValue(json, Map.class);
            return m;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Then("I get an error")
    public void iGetAnError() {
        Assert.assertNotEquals(200, response.statusCode());
    }

    @When("the costumer requests {int} tokens via token")
    public void theCostumerRequestsTokensViaToken(int arg0) {
        TokenFromTokenRequest tokReq = new TokenFromTokenRequest();
        tokReq.token = customer.tokens.get(0);
        tokReq.number = arg0;
        response = client.postJson("tokens/formToken", tokReq);
        if (response.statusCode() == 200) {
            Map<String, Object> m = objectMap(response.body());
            customer.tokens = (ArrayList<String>) m.get("tokens");
        }
    }

    @When("the costumer requests {int} tokens via a random token")
    public void theCostumerRequestsTokensViaARandomToken(int arg0) {
        TokenFromTokenRequest tokReq = new TokenFromTokenRequest();
        tokReq.token = UUID.randomUUID().toString();
        tokReq.number = arg0;
        response = client.postJson("tokens/formToken", tokReq);
        if (response.statusCode() == 200) {
            TokenResponse tokenResponse = client.mapObject(response.body(), TokenResponse.class);
            customer.tokens = (ArrayList<String>) tokenResponse.tokens;
        } else {
            error = response.body();
            System.out.println(error);
        }
    }

    @Then("I get the error {string}")
    public void iGetTheError(String arg0) {
        Assert.assertNotEquals(200, response.statusCode());
        Assert.assertEquals(arg0, error);
    }

    @When("the costumer wants to pay {int} kr to the merchant for a {string}")
    public void theCostumerWantsToPayKrToTheMerchantForA(int arg0, String arg1) {
        TransferRequest tranReq = new TransferRequest();
        tranReq.token = customer.tokens.get(0);
        tranReq.mid = merchant.uid;
        tranReq.amount = arg0;
        tranReq.desc = arg1;

        response = client.postJson("transfer", tranReq);
        if (response.statusCode() != 200) {
            error = response.body();
            System.out.println(error);
        }
    }

    @Then("we see the payment is successful")
    public void weSeeThePaymentIsSuccessful() {
        Assert.assertEquals(200, response.statusCode());
    }

    @When("the customer requests a list of transactions")
    public void theCustomerRequestsAListOfTransactions() {
        response = client.get("report/customer/" + customer.uid);
        if (response.statusCode() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                transactions = mapper.readValue(response.body(), List.class);
                System.out.println(transactions);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @Then("the list of transactions is empty")
    public void theListOfTransactionsIsEmpty() {
        Assert.assertEquals(0, transactions.size());
    }

    @And("the merchant requests a list of transactions")
    public void theMerchantRequestsAListOfTransactions() {
        response = client.get("report/merchant/" + merchant.uid);
        if (response.statusCode() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                transactions = mapper.readValue(response.body(), List.class);
                System.out.println(response.body());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @When("the admin requests a full report")
    public void theAdminRequestsAFullReport() {
        response = client.get("report/");
        if (response.statusCode() == 200) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                transactions = mapper.readValue(response.body(), List.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @When("the costumer want an refund from a purchase with an used token")
    public void theCostumerWantAnRefundFromAPurchaseWithAnUsedToken() {
        RefundRequest refundReq = new RefundRequest();
        refundReq.userId = customer.uid;
        refundReq.token = UUID.fromString(customer.tokens.get(0));

        response = client.postJson("refund", refundReq);
        if (response.statusCode() != 200) {
            error = response.body();
            System.out.println(error);
        }
    }

    @Then("we see the refund is successful")
    public void weSeeTheRefundIsSuccessful() {
        Assert.assertEquals(200, response.statusCode());
    }

    @And("sleep for {int} seconds")
    public void sleepForSeconds(int arg0) throws InterruptedException {
        TimeUnit.SECONDS.sleep(arg0);
    }

    @Then("the list of transactions contains a transaction of {string}")
    public void theListOfTransactionsContainsATransactionOf(String arg0) {
        Assert.assertTrue(transactions.stream().filter(t ->
                t.contains(arg0)
        ).toArray().length >= 1);
    }

    @And("the list of transactions does not contain a transaction of {string}")
    public void theListOfTransactionsDoesNotContainATransactionOf(String arg0) {
        Assert.assertEquals(0, transactions.stream().filter(t ->
                t.contains(arg0)
        ).toArray().length);
    }

    @Given("a brand new customer has a bank account")
    public void aBrandNewCustomerHasABankAccount() {
        try {
            type = "customer";
            User u = new User();
            u.setFirstName("m");
            u.setLastName("l");
            u.setCprNumber("112233-1234");
            bankId = bank.createAccountWithBalance(u, BigDecimal.valueOf(10000.0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }
}


