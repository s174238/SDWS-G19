//==========================================
// Author: Christian
// Date:   Jan 2021
//==========================================
package org.acme.resources;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class RestApplication extends Application {

}
