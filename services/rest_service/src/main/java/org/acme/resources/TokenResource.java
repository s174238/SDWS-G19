//==========================================
// Author: Betta
// Date:   Jan 2021
//==========================================
package org.acme.resources;

import org.acme.businesslogic.TokenService;
import org.acme.models.TokenFromTokenRequest;
import org.acme.models.TokenRequest;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.jboss.logging.annotations.ResolutionDoc;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("tokens")
@Consumes(MediaType.APPLICATION_JSON)
public class TokenResource {
    TokenService service = TokenService.getInstance();
    @POST
    public Response getTokensRequest(@RequestBody TokenRequest request) {
        return service.getTokens(request);
    }
    @POST
    @Path("formToken")
    public Response getTokensFromToken(@RequestBody TokenFromTokenRequest request) {
        return service.getTokensFromToken(request);
    }
}

