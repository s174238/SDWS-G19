package org.acme.models;

import java.util.List;

public class RegisterResponse {
    public String userId;
    public String message;

    public RegisterResponse() {}

    public RegisterResponse(String userId, String message) {
        this.userId = userId;
        this.message = message;
    }
}
