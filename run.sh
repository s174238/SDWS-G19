#!/bin/bash
#==========================================
# Author: Christian
# Date:   Jan 2021
#==========================================

# Start rabbitmq container
docker-compose up --detach --remove-orphans rabbitmq

# Wait 10 seconds for rabbitmq
sleep 10s

# Start all service containers
docker-compose up --detach --remove-orphans
