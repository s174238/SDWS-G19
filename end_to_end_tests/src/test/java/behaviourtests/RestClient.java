//==========================================
// Author: Christian
// Date:   Jan 2021
//==========================================
package behaviourtests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;

public class RestClient {

    private final HttpClient client = HttpClient.newHttpClient();
    private static final String baseuri = "http://localhost:8080/";

    private final ObjectMapper objectMapper = new ObjectMapper();

    public <T> T mapObject(String json, Class<T> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            System.out.println("JsonProcessingException: " + e);
            return null;
        }
    }


    public <T> List<T> mapList(String json) throws IOException {
        return objectMapper.readValue(json, new TypeReference<List<T>>() {
        });
    }

    public HttpResponse post(String path) {
        HttpRequest req = httpBuilder()
                .POST(HttpRequest.BodyPublishers.noBody())
                .uri(URI.create(baseuri + path))
                .header("Accept", "*/*")
                .build();
        return client.sendAsync(req, HttpResponse.BodyHandlers.ofString()).join();
    }

    public <T> HttpResponse postJson(String path, T object) {
        var body = serialiseBody(object);
        System.out.println("postJson.body: " + body);
        HttpRequest req = httpBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(body))
//                .timeout(Duration.ofSeconds(5))
                .uri(URI.create(baseuri + path))
                .header("Accept", "*/*")
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .build();
        return client.sendAsync(req, HttpResponse.BodyHandlers.ofString()).join();
    }


    public HttpResponse<String> get(String path) {
        var req = httpBuilder()
                .GET()
                .uri(URI.create(baseuri + path))
                .header("Accept", "*/*")
                .build();
        return client.sendAsync(req, HttpResponse.BodyHandlers.ofString()).join();
    }

    public HttpResponse<String> delete(String path) {
        var req = httpBuilder()
                .DELETE()
                .uri(URI.create(baseuri + path))
                .header("Accept", "*/*")
                .build();
        return client.sendAsync(req, HttpResponse.BodyHandlers.ofString()).join();
    }

    private HttpRequest.Builder httpBuilder() {
        return HttpRequest.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .timeout(Duration.ofMillis(500));
    }

    private <T> String serialiseBody(T object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
