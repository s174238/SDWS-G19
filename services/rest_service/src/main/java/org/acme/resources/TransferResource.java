//==========================================
// Author: Betta
// Date:   Jan 2021
//==========================================
package org.acme.resources;

import org.acme.businesslogic.TransferService;
import org.acme.models.TransferRequest;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/transfer")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {
    @POST
    public Response transfer(@RequestBody TransferRequest request) {
        TransferService service = TransferService.getInstance();
        return service.transferPayment(request);
    }
}
