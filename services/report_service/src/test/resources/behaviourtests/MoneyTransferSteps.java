package behaviourtests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;

import dtu.ws.fastmoney.BankService;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.EventSender;
import moneytransfer.MoneyTransferService;

public class MoneyTransferSteps {
	
	BankService bank = mock(BankService.class);
	EventSender eventSender = mock(EventSender.class);
	
	private MoneyTransferService moneyTransferService = new MoneyTransferService(bank,eventSender);

	@When("^the service receives a \"([^\"]*)\" event$")
	public void theServiceReceivesAEvent(String arg1) throws Throwable {
		Object[] objs = { "account1", "account2", new BigDecimal(100), "Comment"};
		Event event = new Event(arg1,objs);
		moneyTransferService.receiveEvent(event);
	}
	
	@Then("^it initiates a money transfer with the bank$")
	public void itInitiatesAMoneyTransferWithTheBank() throws Throwable {
		verify(bank).transferMoneyFromTo("account1", "account2", new BigDecimal(100), "Comment");
	}
	
	@Then("^the \"([^\"]*)\" event is broadcast$")
	public void theEventIsBroadcast(String eventType) throws Throwable {
		Event event = new Event(eventType);
		verify(eventSender).sendEvent(event);
	}
	
	
}

