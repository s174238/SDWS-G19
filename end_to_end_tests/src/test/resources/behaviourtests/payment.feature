#==========================================
# Author: Betta
# Date:   Jan 2021
#==========================================
Feature: Payment

  Scenario: Successful Payment
    Given a new customer has a bank account
    When the customer registers with their Bank Account ID as customer
    Then the customer is registered with the Bank Account ID in DTUPay
    When the costumer requests 2 tokens via id
    Then the costumer has 2 unused tokens

    Given a new merchant has a bank account
    When the merchant registers with their Bank Account ID as merchant
    Then the merchant is registered with the Bank Account ID in DTUPay

    When the costumer wants to pay 100 kr to the merchant for a "product"
    Then we see the payment is successful