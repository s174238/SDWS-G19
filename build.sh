#!/bin/bash
#==========================================
# Author: Betta
# Date:   Jan 2021
#==========================================

# Build services
(cd services/rest_service && mvn package -Dquarkus.native.container-build=true)
(cd services/token_service && mvn package)
(cd services/acc_management_service && mvn package)
(cd services/money_transfer_service && mvn package)
(cd services/report_service && mvn package)

# Build all docker images
docker-compose build
