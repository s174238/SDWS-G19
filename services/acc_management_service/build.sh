#!/bin/bash
set -e
mvn clean package
docker build . -t acc_management_service
