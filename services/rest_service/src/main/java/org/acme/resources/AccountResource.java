//==========================================
// Author: Betta
// Date:   Jan 2021
//==========================================
package org.acme.resources;

import org.acme.businesslogic.AccountService;
import org.acme.models.RegisterRequest;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/account")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    private final AccountService service = AccountService.getInstance();

    @POST
    public Response register(@RequestBody RegisterRequest request) {
        return service.createAccount(request);
    }

    @DELETE
    @Path("{userId}")
    public Response delete(@PathParam String accountId) {
        return service.deleteAccount(accountId);
    }
}