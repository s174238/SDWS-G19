#!/bin/bash

# Build quarkus application
mvn package -Dquarkus.native.container-build=true

# Build docker image from application
docker build -t quarkus/rest_service .