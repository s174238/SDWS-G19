package behaviourtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.UUID;

import dtu.bank.*;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import messaging.Event;
import messaging.EventSender;
import moneytransfer.AccountManagementService;

//Author: s174286 Simon S. Brandt

public class AccountManagementServiceSteps {

    BankService bank;

    AccountManagementService ams;
    Event event;
    Event sentEvent;

    String error;
    BankServiceException_Exception bankErr;

    String bankId;
    String type;
    UUID uuid;
    UUID cuuid;
    UUID muuid;

    boolean payment = false;

    String cBankId;
    String mBankId;

    public AccountManagementServiceSteps() {
        bank = new BankServiceService().getBankServicePort();
        ams = new AccountManagementService(new EventSender() {

            @Override
            public void sendEvent(Event ev) throws Exception {
                event = ev;
            }

        });
    }

    @Given("the merchant has a bank account")
    public void theMerchantHasABankAccount() {
        try {
            type = "merchant";
            User u = new User();
            u.setFirstName("bib");
            u.setLastName("bob");
            u.setCprNumber("buup");
            bankId = bank.createAccountWithBalance(u, BigDecimal.valueOf(0.0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @Given("the customer has a bank account")
    public void theCustomerHasABankAccount() {
        try {
            type = "customer";
            User u = new User();
            u.setFirstName("bib");
            u.setLastName("bob");
            u.setCprNumber("buup");
            bankId = bank.createAccountWithBalance(u, BigDecimal.valueOf(0.0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @Given("the customer\\/merchant has a bank account")
    public void theCustomerMerchantHasABankAccount() {
        try {
            type = "customer";
            User u = new User();
            u.setFirstName("bib");
            u.setLastName("bob");
            u.setCprNumber("buup");
            bankId = bank.createAccountWithBalance(u, BigDecimal.valueOf(0.0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @Given("they are not registered with DTUPay")
    public void theyAreNotRegisteredWithDTUPay() {
        assertFalse(ams.getCustomerAccounts().containsValue(bankId));
        assertFalse(ams.getMerchantAccounts().containsValue(bankId));

        if (payment) {
            assertFalse(ams.getMerchantAccounts().containsValue(mBankId) && ams.getCustomerAccounts().containsValue(cBankId));
        }
    }

    @When("I receive create event {string} for that person")
    public void iReceiveCreateEventForThatPerson(String string) {
        Object[] o = {bankId, type};
        sentEvent = new Event(string, o);
        try {
            ams.receiveEvent(sentEvent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("I receive delete event {string} for that person")
    public void iReceiveDeleteEventForThatPerson(String string) {
        Object[] o = {null, type};
        if (uuid != null) {
            o = new Object[]{uuid.toString(), type};
        }
        sentEvent = new Event(string, o);
        try {
            ams.receiveEvent(sentEvent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("I receive create event {string} with argument null")
    public void iReceiveCreateEventWithArgumentNull(String string) {
        Object[] o = {null, type};
        sentEvent = new Event(string, o);
        try {
            ams.receiveEvent(sentEvent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Then("I have sent the event {string}")
    public void iHaveSentTheEvent(String string) {
        assertEquals(string, event.getEventType());
    }

    @Given("they are registered with DTUPay")
    public void theyAreRegisteredWithDTUPay() {
        uuid = ams.addAccount(bankId, type);
        cuuid = ams.addAccount(cBankId, "customer");
        muuid = ams.addAccount(mBankId, "merchant");
    }

    @Then("I have created the DTUPay account")
    public void iHaveCreatedTheDTUPayAccount() {
        uuid = (UUID) event.getArguments()[0];
        if (type == "customer") {
            assertTrue(ams.getCustomerAccounts().containsKey(uuid));
            assertTrue(ams.getCustomerAccounts().get(uuid) == bankId);
        } else {
            assertTrue(ams.getMerchantAccounts().containsKey(uuid));
            assertTrue(ams.getMerchantAccounts().get(uuid) == bankId);
        }
    }

    @Then("I have deleted the DTUPay account")
    public void iHaveDeletedTheDTUPayAccount() {
        if (type == "customer") {
            assertFalse(ams.getCustomerAccounts().containsKey(uuid));
        } else {
            assertFalse(ams.getMerchantAccounts().containsKey(uuid));
        }
    }


    @When("I receive event {string} for that person with amount {int}")
    public void iReceiveEventForAndAmount(String string, Integer int1) throws Exception {
        Object[] o = {null, int1};
        if (uuid != null) {
            o = new Object[]{uuid.toString(), int1};
        }
        sentEvent = new Event(string, o);
        ams.receiveEvent(sentEvent);
    }

    @Then("I have sent the event {string} with their ID and amount {int}")
    public void iHaveSentTheEventWithAndAmount(String string, Integer int1) {
        assertEquals(string, event.getEventType());
        assertEquals(uuid, event.getArguments()[0]);
        assertEquals(int1, event.getArguments()[1]);
    }


    @Given("they are not registered with DTUPay as a customer")
    public void theyAreNotRegisteredWithDTUPayAsACustomer() {
        assertFalse(ams.getCustomerAccounts().containsValue(bankId));

    }

    @Given("the customer and merchant has a bank account")
    public void theCustomerAndMerchantHasABankAccount() {
        payment = true;
        try {
            User u = new User();
            u.setFirstName("q");
            u.setLastName("l");
            u.setCprNumber("m");
            cBankId = bank.createAccountWithBalance(u, BigDecimal.valueOf(0.0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        try {
            User u = new User();
            u.setFirstName("n");
            u.setLastName("o");
            u.setCprNumber("p");
            mBankId = bank.createAccountWithBalance(u, BigDecimal.valueOf(0.0));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @When("I receive the payment event {string} with UUIDs")
    public void iReceiveThePaymentEventWithUUIDs(String string) throws Exception {
        Object[] o = {null, null, null, null, null};
        if (cuuid != null) {
            o = new Object[]{cuuid.toString(), muuid.toString(), null, null, null};
        }
        sentEvent = new Event(string, o);
        ams.receiveEvent(sentEvent);
    }

    @When("I receive the refund event {string} with UUIDs")
    public void iReceiveTheRefundEventWithUUIDs(String string) throws Exception {
        Object[] o = {null, null, null, null, null};
        if (muuid != null) {
            o = new Object[]{muuid.toString(), cuuid.toString(), null, null};
        }
        sentEvent = new Event(string, o);
        ams.receiveEvent(sentEvent);
    }

    @Then("I have sent the payment event {string} with AccountIDs")
    public void iHaveSentThePaymentEventWithAccountIDs(String string) {
        assertEquals(string, event.getEventType());
        assertEquals(cBankId, event.getArguments()[0]);
        assertEquals(mBankId, event.getArguments()[1]);
        payment = false;
    }

    @Then("I have sent the refund event {string} with AccountIDs")
    public void iHaveSentTheRefundEventWithAccountIDs(String string) {
        assertEquals(string, event.getEventType());
        assertEquals(mBankId, event.getArguments()[0]);
        assertEquals(cBankId, event.getArguments()[1]);
        payment = false;
    }


    @After()
    public void clearAccounts() {
        ams.clearAccounts();
        try {
            bank.retireAccount(bankId);
        } catch (BankServiceException_Exception e) {
        }
        try {
            bank.retireAccount(cBankId);
        } catch (BankServiceException_Exception e) {
        }
        try {
            bank.retireAccount(mBankId);
        } catch (BankServiceException_Exception e) {
        }
    }
}

