#!/bin/bash
#==========================================
# Author: Christian
# Date:   Jan 2021
#==========================================

# Build all services
sh build.sh

# Run all services
sh run.sh

# Run all end to end tests
cd end_to_end_tests && mvn clean test -e

# Stop all docker images
docker-compose down

# Remove unused images (cleanup)
docker image prune -f

